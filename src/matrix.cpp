#include <new>
#include <cstring>
#include <algorithm>

#include <qifen/config.h>
#include <qifen/matrix.h>
#include <qifen/interval.h>
#include <qifen/mex-compatible.h>
#include <qifen/interval-internal.hpp>
#include <qifen/matrix-internal.hpp>

#include <kv/eig.hpp>

const size_t qifen_sizeof_matrix = sizeof(::qifen_matrix_t);

using matrix_t = ::qifen_matrix_struct::matrix_t;

namespace qifen {
	namespace internal {
#if !defined(QIFEN_CONFIG_DISABLE_KV)
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
		const ::boost::numeric::ublas::matrix<double> &matrix_to_kv(const ::qifen_matrix_t v)
		{
			return **v;
		}

		::boost::numeric::ublas::matrix<double> &matrix_to_kv(::qifen_matrix_t v)
		{
			return **v;
		}
#  elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
		::boost::numeric::ublas::matrix<double> matrix_to_kv(const ::qifen_matrix_t v)
		{
			auto nrows = ::mxGetM(v->value);
			auto ncols = ::mxGetN(v->value);

			::boost::numeric::ublas::matrix<double> r(nrows, ncols);

			::std::memcpy(r.data().begin(), ::mxGetPr(v), sizeof(double) * nrows * ncols);

			return r;
		}
#  endif
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
		::mxArray *matrix_to_intlab(const ::qifen_matrix_t v)
		{
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
			size_t row = (*v)->size1(), col = (*v)->size2();
			auto r = ::mxCreateDoubleMatrix(row, col);
			:std::memcpy(::mxGetPr(r), (*v)->data(), sizeof(double) * row * col);
			return r;
#  elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
			return const_cast<::mxArray*>(v->value);
#  endif
		}
#endif

#if !defined(QIFEN_CONFIG_DISABLE_KV)
		template <typename T>
		void matrix_set(::qifen_matrix_t dest, const ::boost::numeric::ublas::matrix_expression<T> &v)
		{
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
			**dest = v;
#  elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
			::mxDestroyArray(dest->value);
			dest->value = ::mxCreateDoubleMatrix(v.size1(), v.size2(), mxREAL);

			auto p = ::mxGetPr(dest->value);
			auto vv = v();
			auto nrows = vv.size1(), ncols = vv.size2();

			for (size_t i = 0; i < ncols; ++i) {
				for (size_t j = 0; j < nrows; ++j) {
					p[j + i * nrows] = vv(j, i);
				}
			}
#  endif
		}

		void matrix_set(::qifen_matrix_t dest, const ::boost::numeric::ublas::matrix<double> &v)
		{
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
			**dest = v;
#  elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
			::mxDestroyArray(dest->value);
			dest->value = ::mxCreateDoubleMatrix(v.size1(), v.size2(), mxREAL);

			auto p = ::mxGetPr(dest->value);
			auto nrows = v.size1(), ncols = v.size2();

			for (size_t i = 0; i < ncols; ++i) {
				for (size_t j = 0; j < nrows; ++j) {
					p[j + i * nrows] = v(j, i);
				}
			}
#  endif
		}
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
		void matrix_set(::qifen_matrix_t dest, ::mxArray *v)
		{
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
			auto nrows = ::mxGetM(v);
			auto ncols = ::mxGetN(v);
			auto &m = **dest;
			auto p - ::mxGetPr(v);

			m.resize(nrow, ncols);

			for (size_t i = 0; i < ncols; ++i) {
				for (size_t j = 0; j < nrows; ++j) {
					m(j, i) = p[j + i * nrows];
				}
			}
#  elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
			::mxDestroyArray(dest->value);
			::mexCallMATLAB(1, &dest->value, 1, v, "double")
#  endif
		}
#endif
	}
}

void qifen_matrix_init(qifen_matrix_t v, size_t row, size_t col)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	new(&v->storage) matrix_t(row, col, 0);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	v->value = ::mxCreateDoubleMatrix(row, col, mxREAL);
#endif
}

void qifen_matrix_clear(qifen_matrix_t v)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	(*v)->~matrix_t();
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::mxDestroyArray(v->value);
#endif
}

void qifen_matrix_get_size(const qifen_matrix_t v, size_t *nrows, size_t *ncols)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	if (nrows != nullptr)
		*nrows = (*v)->size1();
	if (ncols != nullptr)
		*ncols = (*v)->size2();
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	if (nrows != nullptr)
		*nrows = ::mxGetM(v->value);
	if (ncols != nullptr)
		*ncols = ::mxGetN(v->value);
#endif
}

void qifen_matrix_set(qifen_matrix_t dest, const qifen_matrix_t v)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	**dest = **v;
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen::internal::matrix_set(dest, v->value);
#endif
}

void qifen_matrix_set_entry(qifen_matrix_t dest, size_t row, size_t col, double v)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	(**dest)(row, col) = v;
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::mxGetPr(dest->value)[row + col * nrows] = v;
#endif
}

void qifen_matrix_get_entry(const qifen_matrix_t dest, size_t row, size_t col, double *v)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	if (v != nullptr)
		*v = (**dest)(row, col);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	if (v != nullptr)
		*v = ::mxGetPr(dest->value)[row + col * nrows];
#endif
}

void qifen_matrix_resize(qifen_matrix_t v, size_t row, size_t col)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	if ((*v)->size1() != row || (*v)->size2() != col)
		(*v)->resize(row, col);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	static_assert("not implemented yet");
#endif
}

void qifen_matrix_transpose(qifen_matrix_t dest, const qifen_matrix_t a)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_matrix_kv_transpose(dest, a);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_matrix_intlab_transpose(dest, a);
#endif
}

void qifen_matrix_negate(qifen_matrix_t dest, const qifen_matrix_t a)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_matrix_kv_negate(dest, a);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_matrix_intlab_negate(dest, a);
#endif
}

void qifen_matrix_add(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_matrix_kv_add(dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_matrix_intlab_add(dest, a, b);
#endif
}

void qifen_matrix_sub(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_matrix_kv_sub(dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_matrix_intlab_sub(dest, a, b);
#endif
}

void qifen_matrix_mul(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_matrix_kv_mul(dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_matrix_intlab_mul(dest, a, b);
#endif
}

void qifen_matrix_mul_scalar(qifen_matrix_t dest, const qifen_matrix_t a, double b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_matrix_kv_mul_scalar(dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_matrix_intlab_mul_scalar(dest, a, b);
#endif
}

qifen_error_t qifen_matrix_verify_eig_minmax_real(qifen_interval_t dest, const qifen_matrix_t a, qifen_verify_eig_method_t method)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	return ::qifen_matrix_kv_verify_eig_minmax_real(dest, a, method);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	return ::qifen_matrix_intlab_verify_eig_minmax_real(dest, a, method);
#endif
}

#if !defined(QIFEN_CONFIG_DISABLE_KV)
void qifen_matrix_set_kv(qifen_matrix_t dest, ::boost::numeric::ublas::matrix<double> &v)
{
	::qifen::internal::matrix_set(dest, v);
}

void qifen_matrix_kv_transpose(qifen_matrix_t dest, const qifen_matrix_t a)
{
	QIFEN_VAR_KV_MATRIX(aa, a);
	::qifen::internal::matrix_set(dest, trans(aa));
}

void qifen_matrix_kv_negate(qifen_matrix_t dest, const qifen_matrix_t a)
{
	QIFEN_VAR_KV_MATRIX(aa, a);
	::qifen::internal::matrix_set(dest, -aa);
}

void qifen_matrix_kv_add(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b)
{
	QIFEN_VAR_KV_MATRIX(aa, a);
	QIFEN_VAR_KV_MATRIX(bb, b);
	::qifen::internal::matrix_set(dest, aa + bb);
}

void qifen_matrix_kv_sub(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b)
{
	QIFEN_VAR_KV_MATRIX(aa, a);
	QIFEN_VAR_KV_MATRIX(bb, b);
	::qifen::internal::matrix_set(dest, aa - bb);
}

void qifen_matrix_kv_mul(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b)
{
	QIFEN_VAR_KV_MATRIX(aa, a);
	QIFEN_VAR_KV_MATRIX(bb, b);
	::qifen::internal::matrix_set(dest, prod(aa, bb));
}

void qifen_matrix_kv_mul_scalar(qifen_matrix_t dest, const qifen_matrix_t a, double b)
{
	QIFEN_VAR_KV_MATRIX(aa, a);
	::qifen::internal::matrix_set(dest, aa * b);
}

qifen_error_t qifen_matrix_kv_verify_eig_minmax_real(qifen_interval_t dest, const qifen_matrix_t a, qifen_verify_eig_method_t method)
{
	size_t row, col;
	::qifen_matrix_get_size(a, &row, &col);

#  if !defined(NDEBUG)
	if (row != col) {
		return ::qifen_error_matrix_size;
	}
#  endif

	if (row == 1 && col == 1) {
		double v;
		::qifen_matrix_get_entry(a, 0, 0, &v);
		::qifen_interval_set_infsup(dest, v, v);
		return ::qifen_error_succeeded;
	}

	if (method == qifen_verify_eig_library_dependent) {
		::boost::numeric::ublas::vector<::kv::complex<::kv::interval<double>>> v;

		if (!::kv::veig(::qifen::internal::matrix_to_kv(a), v) || v.size() == 0) {
			return qifen_error_eig;
		}

		double inf = v(0).real().lower(), sup = v(0).real().upper();

		for (size_t i = 1; i < v.size(); ++i) {
			inf = ::std::min(inf, v(i).real().lower());
			sup = ::std::max(sup, v(i).real().upper());
		}

		::qifen_interval_set_infsup(dest, inf, sup);

		return qifen_error_succeeded;
	} else if (method == qifen_verify_eig_auto || method == qifen_verify_eig_gershgorin) {
		QIFEN_VAR_KV_MATRIX(aa, a);
		double inf = aa(0, 0), sup = aa(0, 0);

		for (size_t i = 0; i < aa.size1(); ++i) {
			::kv::interval<double> c(aa(i, i));
			for (size_t j = 0; j < aa.size2(); ++j) {
				if (i != j) {
					auto v = ::std::abs(aa(i, j));
					c += ::kv::interval<double>(-v, v);
				}
			}
			inf = ::std::min(inf, c.lower());
			sup = ::std::max(sup, c.upper());
		}

		::qifen_interval_set_infsup(dest, inf, sup);

		return qifen_error_succeeded;
	} else if (method == qifen_verify_eig_jacobi_gershgorin) {
		// TODO
		return qifen_error_eig;
	}

	return qifen_error_succeeded;
}

void qifen_matrix_to_kv(const qifen_matrix_t v, ::boost::numeric::ublas::matrix<double> &dest)
{
	dest = ::qifen::internal::matrix_to_kv(v);
}
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
void qifen_matrix_intlab_transpose(qifen_matrix_t dest, const qifen_matrix_t a)
{
	QIFEN_VAR_INTLAB_MATRIX(aa, a);
	QIFEN_MEX_LHS_AS_DEST_MATRIX(lhs, dest);
	::mexCallMATLAB(1, &lhs, 1, &aa, "transpose");
}
void qifen_matrix_intlab_transpose(qifen_matrix_t dest, const qifen_matrix_t a)
{
	QIFEN_VAR_INTLAB_MATRIX(aa, a);
	QIFEN_MEX_LHS_AS_DEST_MATRIX(lhs, dest);
	::mexCallMATLAB(1, &lhs, 1, &aa, "uminus");
}

void qifen_matrix_intlab_add(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b)
{
	QIFEN_VAR_INTLAB_MATRIX(aa, a);
	QIFEN_VAR_INTLAB_MATRIX(bb, b);
	QIFEN_MEX_LHS_AS_DEST_MATRIX(lhs, dest);
	::mxArray *rhs[] = { aa, bb };
	::mexCallMATLAB(1, &lhs, 2, rhs, "plus");
}

void qifen_matrix_intlab_sub(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b)
{
	QIFEN_VAR_INTLAB_MATRIX(aa, a);
	QIFEN_VAR_INTLAB_MATRIX(bb, b);
	QIFEN_MEX_LHS_AS_DEST_MATRIX(lhs, dest);
	::mxArray *rhs[] = { aa, bb };
	::mexCallMATLAB(1, &lhs, 2, rhs, "minus");
}

void qifen_matrix_intlab_mul(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b)
{
	QIFEN_VAR_INTLAB_MATRIX(aa, a);
	QIFEN_VAR_INTLAB_MATRIX(bb, b);
	QIFEN_MEX_LHS_AS_DEST_MATRIX(lhs, dest);
	::mxArray *rhs[] = { aa, bb };
	::mexCallMATLAB(1, &lhs, 2, rhs, "mtimes");
}

void qifen_matrix_intlab_mul_scalar(qifen_matrix_t dest, const qifen_matrix_t a, double b)
{
	QIFEN_VAR_INTLAB_MATRIX(aa, a);
	QIFEN_MEX_LHS_AS_DEST_MATRIX(lhs, dest);
	::mxArray *rhs[] = { aa, ::mxCreateDoubleScalar(b) };
	::mexCallMATLAB(1, &lhs, 2, rhs, "times");
	::mxDestroyArray(rhs[1]);
}

qifen_error_t qifen_matrix_intlab_verify_eig_minmax_real(qifen_interval_t dest, const qifen_matrix_t a,
	qifen_verify_eig_method_t method)
{
	// TODO
	return ::qifen_matrix_kv_verify_eig_minmax_real(dest, a, method);
}

void qifen_matrix_to_intlab(const qifen_matrix_t v, mxArray **dest)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	*dest = ::qifen::internal::matrix_to_intlab(v);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	*dest = ::mxDuplicateArray(v->value);
#endif
}
#endif
