#define BOOST_UBLAS_MOVE_SEMANTICS

#include <tuple>
#include <algorithm>
#include <limits>
#include <stdexcept>
#include <vector>

#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/odeint/util/ublas_wrapper.hpp>

#include <qifen/qi.h>
#include <qifen/interval.h>
#include <qifen/interval_matrix.h>
#include <qifen/matrix.h>
#include <qifen/config.h>
#include <qifen/internal.hpp>
#include <qifen/qi-internal.hpp>
#include <qifen/matrix-internal.hpp>
#include <qifen/interval-internal.hpp>
#include <qifen/interval_matrix-internal.hpp>

#include <kv/interval-vector.hpp>
#include <kv/rdouble.hpp>

// #include <iostream>

namespace qifen {
	namespace internal {
		::std::pair<double, double> verify_linpart_range(qifen_qi_context_t ctx, const qifen_matrix_t a)
		{
			QIFEN_VAR_KV_MATRIX(aa, a);
			::kv::interval<double> r(0.0);
			for (size_t i = 0; i < aa.size1(); ++i) {
				auto t = ::std::abs(aa(i, 0));
				r += ::kv::interval<double>(-t, t);
			}
			return ::std::make_pair(r.lower(), r.upper());
		}

		::std::pair<double, double> verify_quadpart_range(qifen_qi_context_t ctx, const qifen_matrix_t a)
		{
			::qifen_interval_t r;

			::qifen_interval_init(r);

			size_t size;
			::qifen_matrix_get_size(a, &size, nullptr);

			double k = static_cast<double>(size);
			::qifen_interval_t t;

			::qifen_interval_init(t);

			for (size_t i = 0; i < size; ++i) {
				for (size_t j = i; j < size; ++j) {
					double x;
					::qifen_matrix_get_entry(a, i, j, &x);

					if (i == j) {
						if (x < 0.0) {
							::qifen_interval_set_infsup(t, x, 0.0);
						} else {
							::qifen_interval_set_infsup(t, 0.0, x);
						}
					} else {
						if (x < 0.0) {
							::qifen_interval_set_infsup(t, 2.0 * x, -2.0 * x);
						} else {
							::qifen_interval_set_infsup(t, -2.0 * x, 2.0 * x);
						}
					}

					::qifen_interval_add(r, r, t);
				}
			}

			::qifen_interval_clear(t);

			double inf, sup;

			::qifen_interval_get_infsup(r, &inf, &sup);
			::qifen_interval_clear(r);

			return ::std::make_pair(::std::min(inf, 0.0), sup);
		}

		::std::pair<double, double> verify_qi_quadpart_range(
			qifen_qi_context_t ctx, qifen_qi_t qi)
		{
			if (qi->is_quad_zero) {
				return ::std::make_pair(0.0, 0.0);
			}

			if (qi->has_quad_minmax) {
				return ::std::make_pair(qi->quad_min, qi->quad_max);
			}

			auto r = verify_quadpart_range(ctx, qi->A);

			qi->has_quad_minmax = true;
			qi->quad_min = r.first;
			qi->quad_max = r.second;

			return r;
		}

		::std::pair<double, double> verify_qi_range(
			qifen_qi_context_t ctx, qifen_qi_t qi)
		{
			::kv::interval<double> range(qi->x0);
			double a, b;
			::std::tie(a, b) = verify_linpart_range(ctx, qi->a);
			range += ::kv::interval<double>(a, b);
			::std::tie(a, b) = verify_qi_quadpart_range(ctx, qi);
			range += ::kv::interval<double>(a, b);
			return ::std::make_pair(range.lower(), range.upper());
		}
	}
}

namespace {
	constexpr double pi = 3.1415926535897932384626;

	void pad_zero(qifen_qi_t a, size_t n_old, size_t n)
	{
		for (size_t i = n_old; i < n; ++i) {
			::qifen_matrix_set_entry(a->a, i, 0, 0.0);
			for (size_t j = 0; j <= i; ++j) {
				::qifen_matrix_set_entry(a->A, i, j, 0.0);
				::qifen_matrix_set_entry(a->A, j, i, 0.0);
			}
		}
	}

	namespace inv {
		template <typename T>
		T calc_phi(const T &r, const T &p)
		{
			using ::std::atan2;
			using ::std::sqrt;
			using ::std::pow;
			return atan2(sqrt(-(pow(r, 2) + pow(p / 3.0, 3))), -r);
		}

		template <typename T>
		T calc_x(const T &phi, const T &p, size_t i)
		{
			using ::std::sqrt;
			using ::std::cos;
			using ::std::pow;
			// std::cout << "phi = " << phi << std::endl;
			// std::cout << "cos = " << cos((phi + i * 2 * ::kv::constants<T>::pi()) / 3.0) << std::endl;
			return pow(2.0 * sqrt(-p / 3.0) * cos((phi + i * 2 * ::kv::constants<T>::pi()) / 3.0), -1);
		}

		template <typename T>
		T quad_approx(const T &r, const T&p, const T &q, const T &x)
		{
			using ::std::pow;
			return r * pow(x, 2) + p * x + q;
		}
	}
}

const qifen_qi_inv_config_ptr qifen_qi_inv_config_default()
{
	static qifen_qi_inv_config_struct s{
		::qifen_approx_best_effort,
		3,
		0.0,
	};

	return &s;
}

qifen_qi_ptr qifen_qi_new(qifen_qi_context_t ctx)
{
	::qifen_qi_ptr p = new ::qifen_qi_struct;
	::qifen_qi_init(ctx, p);
	return p;
}

void qifen_qi_delete(qifen_qi_context_t ctx, qifen_qi_t v)
{
	::qifen_qi_clear(ctx, v);
	delete v;
}

void qifen_qi_init(qifen_qi_context_t ctx, qifen_qi_t v)
{
	v->x0 = 0.0;
	::qifen_matrix_init(v->a, ctx->num_dummy, 1);
	::qifen_matrix_init(v->A, ctx->num_dummy, ctx->num_dummy);
	v->delta = 0.0;
	v->quad_min = 0.0;
	v->quad_max = 0.0;
	v->is_quad_zero = true;
	v->has_quad_minmax = true;
}

void qifen_qi_init_interval(qifen_qi_context_t ctx, qifen_qi_t v, const qifen_interval_t a)
{
	double mid, rad;

	::qifen_interval_get_midrad(a, &mid, &rad);

	if (rad == 0.0) {
		::qifen_qi_init(ctx, v);
		v->x0 = mid;
	} else {
		++ctx->num_dummy;
		::qifen_qi_init(ctx, v);
		v->x0 = mid;
		::qifen_matrix_set_entry(v->a, ctx->num_dummy - 1, 0, rad);
	}
}

void qifen_qi_init_infsup(qifen_qi_context_t ctx, qifen_qi_t v, double inf, double sup)
{
	::qifen_interval_t a;
	::qifen_interval_init_infsup(a, inf, sup);
	::qifen_qi_init_interval(ctx, v, a);
	::qifen_interval_clear(a);
}

void qifen_qi_init_infsup_string(qifen_qi_context_t ctx, qifen_qi_t v, const char *inf, const char *sup)
{
	double i, s;

	if (inf != nullptr) {
		::qifen::internal::range<const char> range = { inf, ::std::strchr(inf, '\0') };
		i = ::qifen::internal::stod(range, nullptr, -1);

		if (sup == nullptr) {
			s = ::qifen::internal::stod(range, nullptr, 1);
		} else {
			range = { sup, ::std::strchr(sup, '\0') };
			s = ::qifen::internal::stod(range, nullptr, 1);
		}
	} else if (sup != nullptr) {
		::qifen::internal::range<const char> range = { sup, ::std::strchr(sup, '\0') };
		i = ::qifen::internal::stod(range, nullptr, -1);
		s = ::qifen::internal::stod(range, nullptr, 1);
	} else {
		i = 0.0;
		s = 0.0;
	}

	::qifen_qi_init_infsup(ctx, v, i, s);
}

void qifen_qi_clear(qifen_qi_context_t ctx, qifen_qi_t v)
{
	::qifen_matrix_clear(v->a);
	::qifen_matrix_clear(v->A);
}

void qifen_qi_set(qifen_qi_context_t ctx, qifen_qi_t dest, const qifen_qi_t v)
{
	if (dest == v)
		return;

	dest->x0 = v->x0;
	::qifen_matrix_set(dest->a, v->a);
	::qifen_matrix_set(dest->A, v->A);
	dest->delta = v->delta;
	dest->quad_min = v->quad_min;
	dest->quad_max = v->quad_max;
	dest->is_quad_zero = v->is_quad_zero;
	dest->has_quad_minmax = v->has_quad_minmax;
}

void qifen_qi_set_infsup(qifen_qi_context_t ctx, qifen_qi_t v, double inf, double sup)
{
	double mid = (inf + sup) * 0.5;
	double rad = ::qifen::internal::add_up(sup, -inf) * 0.5;

	if (rad == 0.0) {
		size_t m;
		::qifen_matrix_get_size(v->a, &m, nullptr);
		::pad_zero(v, 0, m);
		v->x0 = mid;
	} else {
		++ctx->num_dummy;

		::qifen_matrix_resize(v->a, ctx->num_dummy, 1);
		::qifen_matrix_resize(v->A, ctx->num_dummy, ctx->num_dummy);
		::pad_zero(v, 0, ctx->num_dummy);

		v->x0 = mid;
		::qifen_matrix_set_entry(v->a, ctx->num_dummy - 1, 0, rad);
	}

	v->quad_min = 0.0;
	v->quad_max = 0.0;
	v->is_quad_zero = true;
	v->has_quad_minmax = true;
}

void qifen_qi_set_infsup_string(qifen_qi_context_t ctx, qifen_qi_t v, const char *inf, const char *sup)
{
	double i, s;

	if (inf != nullptr) {
		::qifen::internal::range<const char> range = { inf, ::std::strchr(inf, '\0') };
		i = ::qifen::internal::stod(range, nullptr, -1);

		if (sup == nullptr) {
			s = ::qifen::internal::stod(range, nullptr, 1);
		} else {
			range = { sup, ::std::strchr(sup, '\0') };
			s = ::qifen::internal::stod(range, nullptr, 1);
		}
	} else if (sup != nullptr) {
		::qifen::internal::range<const char> range = { sup, ::std::strchr(sup, '\0') };
		i = ::qifen::internal::stod(range, nullptr, -1);
		s = ::qifen::internal::stod(range, nullptr, 1);
	} else {
		i = 0.0;
		s = 0.0;
	}

	::qifen_qi_set_infsup(ctx, v, i, s);
}

void qifen_qi_get_infsup(qifen_qi_context_t ctx, qifen_qi_t v, double *inf, double *sup)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	auto r = ::qifen::internal::verify_qi_range(ctx, v);

	if (inf != nullptr) {
		*inf = r.first;
	}
	if (sup != nullptr) {
		*sup = r.second;
	}
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	assert("not implemented");
#endif
}

void qifen_qi_negate(qifen_qi_context_t ctx, qifen_qi_t dest, const qifen_qi_t a)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	qifen_qi_kv_negate(ctx, dest, a);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
#endif
}

void qifen_qi_add_scalar(qifen_qi_context_t ctx, qifen_qi_t dest, const qifen_qi_t a, double b)
{
	if (dest != a) {
		::qifen_qi_set(ctx, dest, a);
	}

	auto r = ::qifen::internal::twosum(a->x0, b);

	dest->x0 = r.first;
	dest->delta = ::qifen::internal::add_up(a->delta, ::std::abs(r.second));
}

void qifen_qi_add(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	qifen_qi_kv_add(ctx, dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
#endif
}

void qifen_qi_sub(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	qifen_qi_kv_sub(ctx, dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
#endif
}

void qifen_qi_mul_scalar(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, double b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	qifen_qi_kv_mul_scalar(ctx, dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
#endif
}

void qifen_qi_mul_interval(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, const qifen_interval_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	qifen_qi_kv_mul_interval(ctx, dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
#endif
}

void qifen_qi_mul(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	qifen_qi_kv_mul(ctx, dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
#endif
}

qifen_error_t qifen_qi_div(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b, const qifen_qi_inv_config_t config)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	return qifen_qi_kv_div(ctx, dest, a, b, config);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
#endif
}

qifen_error_t qifen_qi_inv(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, const qifen_qi_inv_config_t config)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	return qifen_qi_kv_inv(ctx, dest, a, config);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
#endif
}

#if !defined(QIFEN_CONFIG_DISABLE_KV)
void qifen_qi_kv_negate(qifen_qi_context_t ctx, qifen_qi_t dest, const qifen_qi_t a)
{
	dest->x0 = -a->x0;
	::qifen_matrix_negate(dest->a, a->a);

	if (!(dest->is_quad_zero = a->is_quad_zero))
		::qifen_matrix_negate(dest->A, a->A);

	dest->has_quad_minmax = false;
}

void qifen_qi_kv_add_scalar(qifen_qi_context_t ctx, qifen_qi_t dest, const qifen_qi_t a, double b)
{
	::qifen_qi_add_scalar(ctx, dest, a, b);
}

void qifen_qi_kv_add(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b)
{
	{
		size_t a_size, b_size;

		::qifen_matrix_get_size(a->a, &a_size, nullptr);
		::qifen_matrix_get_size(b->a, &b_size, nullptr);

		::qifen_matrix_resize(a->a, ctx->num_dummy, 1);
		::qifen_matrix_resize(a->A, ctx->num_dummy, ctx->num_dummy);
		::qifen_matrix_resize(b->a, ctx->num_dummy, 1);
		::qifen_matrix_resize(b->A, ctx->num_dummy, ctx->num_dummy);
		::qifen_matrix_resize(dest->a, ctx->num_dummy, 1);
		::qifen_matrix_resize(dest->A, ctx->num_dummy, ctx->num_dummy);

		::pad_zero(a, a_size, ctx->num_dummy);
		::pad_zero(b, b_size, ctx->num_dummy);
	}

	QIFEN_VAR_KV_MATRIX(a_a, a->a);
	QIFEN_VAR_KV_MATRIX(b_a, b->a);

	{
		double err;
		::std::tie(dest->x0, err) = ::qifen::internal::twosum(a->x0, b->x0);
		dest->delta = ::qifen::internal::add_up(a->delta, b->delta);
		if (err != 0.0) {
			dest->delta = ::qifen::internal::add_up(dest->delta, ::std::abs(err));
		}
	}

	::boost::numeric::ublas::matrix<::kv::interval<double>> c = a_a;

	for (size_t i = 0; i < c.size1(); ++i) {
		auto t = c(i, 0) + b_a(i, 0);
		::qifen_matrix_set_entry(dest->a, i, 0, mid(t));
		dest->delta = ::qifen::internal::add_up(dest->delta, rad(t));
	}

	if (!a->is_quad_zero && b->is_quad_zero) {
		dest->is_quad_zero = false;
		::qifen_matrix_set(dest->A, a->A);

		dest->has_quad_minmax = a->has_quad_minmax;

		if (b->has_quad_minmax) {
			dest->quad_min = a->quad_min;
			dest->quad_max = a->quad_max;
		}
	} else if (a->is_quad_zero && !b->is_quad_zero) {
		dest->is_quad_zero = false;
		::qifen_matrix_set(dest->A, b->A);

		if (dest->has_quad_minmax = b->has_quad_minmax) {
			dest->quad_min = b->quad_min;
			dest->quad_max = b->quad_max;
		}
	} else if (!a->is_quad_zero && !b->is_quad_zero) {
		dest->is_quad_zero = false;

		QIFEN_VAR_KV_MATRIX(a_A, a->A);
		QIFEN_VAR_KV_MATRIX(b_A, b->A);

		c = a_A;
		c += b_A;

		double round_max = 0.0;

		for (size_t i = 0; i < c.size1(); ++i) {
			double t = rad(c(i, i));
			::qifen::internal::round_to_up([&]() {
				for (size_t j = 0; j < c.size2(); ++j) {
					::qifen_matrix_set_entry(dest->A, i, j, mid(c(i, j)));
					if (i != j) {
						// t += rad(c(i, j));
						t += (c(i, j).upper() - c(i, j).lower()) * 0.5;
					}
				}
			});
			round_max = std::max(round_max, t);
		}

		dest->delta = ::qifen::internal::add_up(dest->delta, round_max);
		dest->has_quad_minmax = false;
	} else {
		dest->is_quad_zero = true;
		dest->has_quad_minmax = true;
	}
}

void qifen_qi_kv_sub(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b)
{
	::qifen_qi_t tmp;
	::qifen_qi_ptr p = dest;

	if (dest == a || dest == b) {
		::qifen_qi_init(ctx, tmp);
		p = tmp;
	}

	::qifen_qi_kv_negate(ctx, p, b);
	::qifen_qi_kv_add(ctx, dest, a, p);

	if (dest == a || dest == b) {
		::qifen_qi_clear(ctx, tmp);
	}
}

void qifen_qi_kv_mul_scalar(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, double b)
{
	size_t size;
	::qifen_matrix_get_size(a->a, &size, nullptr);
	::qifen_matrix_resize(a->a, ctx->num_dummy, 1);
	::qifen_matrix_resize(a->A, ctx->num_dummy, ctx->num_dummy);
	::pad_zero(a, size, ctx->num_dummy);

	::qifen_matrix_resize(dest->a, ctx->num_dummy, 1);
	::qifen_matrix_resize(dest->A, ctx->num_dummy, ctx->num_dummy);

	double err = 0.0;

	::std::tie(dest->x0, err) = ::qifen::internal::twoproduct(a->x0, b);

	::std::vector<double> gershgorin(ctx->num_dummy);

	for (size_t i = 0; i < ctx->num_dummy; ++i) {
		double a_;
		double t;

		::qifen_matrix_get_entry(a->a, i, 0, &a_);
		::std::tie(a_, t) = ::qifen::internal::twoproduct(a_, b);
		t = ::std::abs(t);
		::qifen_matrix_set_entry(dest->a, i, 0, a_);
		err = ::qifen::internal::add_up(err, t);

		if (!a->is_quad_zero) {
			for (size_t j = i; j < ctx->num_dummy; ++j) {
				::qifen_matrix_get_entry(a->A, i, j, &a_);
				::std::tie(a_, t) = ::qifen::internal::twoproduct(a_, b);
				::qifen_matrix_set_entry(dest->A, i, j, a_);
				gershgorin[i] = ::qifen::internal::add_up(gershgorin[i], t);
				if (i != j) {
					::qifen_matrix_set_entry(dest->A, j, i, a_);
					gershgorin[j] = ::qifen::internal::add_up(gershgorin[j], t);
				}
			}
		} else {
			for (size_t j = i; j < ctx->num_dummy; ++j) {
				::qifen_matrix_set_entry(dest->A, i, j, 0.0);
			}
		}
	}

	::qifen::internal::round_to_up([&]() {
		if (gershgorin.size() != 0)
			err += *::std::max_element(gershgorin.begin(), gershgorin.end());
		dest->delta = a->delta * b + err;
	});

	dest->has_quad_minmax = false;
}

void qifen_qi_kv_mul_interval(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, const qifen_interval_t b)
{
	assert("qifen_qi_kv_mul_interval is not implemented");
}

void qifen_qi_kv_mul(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b)
{
	++ctx->num_dummy;

	{
		size_t n;
		::qifen_matrix_get_size(a->a, &n, nullptr);
		::qifen_matrix_resize(a->a, ctx->num_dummy, 1);
		::qifen_matrix_resize(a->A, ctx->num_dummy, ctx->num_dummy);
		::pad_zero(a, n, ctx->num_dummy);
	}

	{
		size_t n;
		::qifen_matrix_get_size(b->a, &n, nullptr);
		::qifen_matrix_resize(b->a, ctx->num_dummy, 1);
		::qifen_matrix_resize(b->A, ctx->num_dummy, ctx->num_dummy);
		::pad_zero(b, n, ctx->num_dummy);
	}

	auto a_lin_range = ::qifen::internal::verify_linpart_range(ctx, a->a);
	auto a_quad_range = ::qifen::internal::verify_qi_quadpart_range(ctx, a);
	auto b_lin_range = ::qifen::internal::verify_linpart_range(ctx, b->a);
	auto b_quad_range = ::qifen::internal::verify_qi_quadpart_range(ctx, b);

	QIFEN_VAR_KV_MATRIX(a_a, a->a);
	QIFEN_VAR_KV_MATRIX(a_A, a->A);
	QIFEN_VAR_KV_MATRIX(b_a, b->a);
	QIFEN_VAR_KV_MATRIX(b_A, b->A);

	::boost::numeric::ublas::matrix<double> new_a(ctx->num_dummy, 1, 0.0), new_A(ctx->num_dummy, ctx->num_dummy, 0.0);

	double err = 0.0;

	// linear terms
	for (size_t i = 0; i < ctx->num_dummy - 1; ++i) {
		double a_, b_;

		::qifen_matrix_get_entry(a->a, i, 0, &a_);
		::qifen_matrix_get_entry(b->a, i, 0, &b_);

		auto r1 = ::qifen::internal::twoproduct(a->x0, b_);
		auto r2 = ::qifen::internal::twoproduct(b->x0, a_);
		auto r3 = ::qifen::internal::twosum(r1.first, r2.first);

		err = ::qifen::internal::add_up(err, ::std::abs(r1.second));
		err = ::qifen::internal::add_up(err, ::std::abs(r2.second));
		err = ::qifen::internal::add_up(err, ::std::abs(r3.second));

		new_a(i, 0) = r3.first;
	}

	// quadratic terms

	::boost::numeric::ublas::matrix<double> higher_terms(ctx->num_dummy, ctx->num_dummy);

	{
		::boost::numeric::ublas::matrix<::kv::interval<double>> A, eT(1, ctx->num_dummy), ee(ctx->num_dummy, ctx->num_dummy), tmp;
		for (size_t i = 0; i < ctx->num_dummy; ++i) {
			eT(0, i) = ::kv::interval<double>(-1.0, 1.0);
			for (size_t j = i; j < ctx->num_dummy; ++j) {
				if (i == 0) {
					ee(i, j) = ::kv::interval<double>(0.0, 1.0);
				} else {
					ee(i, j) = ::kv::interval<double>(-1.0, 1.0);
				}
			}
		}
		tmp = prod(a_a, eT);
		A = prod(tmp, b_A);
		tmp = prod(b_a, eT);
		A += prod(tmp, a_A);
		tmp = prod(a_A, ee);
		A += prod(tmp, b_A);
		tmp = a_a;
		A += prod(tmp, trans(b_a));
		tmp = b_A;
		A += ::kv::interval<double>(a->x0) * tmp;
		tmp = a_A;
		A += ::kv::interval<double>(b->x0) * tmp;

		A = (A + trans(A)) * ::kv::interval<double>(0.5);

		for (size_t i = 0; i < ctx->num_dummy; ++i) {
			for (size_t j = 0; j < ctx->num_dummy; ++j) {
				double m, r;
				midrad(A(i, j), m, r);
				new_A(i, j) = m;
				higher_terms(i, j) = r;
			}
		}
	}

	// error estimation of higher order terms
	::qifen::internal::round_to_up([&]() {
		double higher_err = 0.0;

		for (size_t i = 0; i < ctx->num_dummy - 1; ++i) {
			auto s = sum(row(higher_terms, i));
			higher_err += s;
		}

		err += higher_err;
		err += a->delta * (b->x0 + b_lin_range.second
			+ ::std::max(::std::abs(b_quad_range.first), ::std::abs(b_quad_range.second)) + b->delta);
		err += b->delta * (a->x0 + a_lin_range.second
			+ ::std::max(::std::abs(a_quad_range.first), ::std::abs(a_quad_range.second)));
	});

	double round_err = 0.0;
	::std::tie(dest->x0, round_err) = ::qifen::internal::twoproduct(a->x0, b->x0);

	new_a(ctx->num_dummy - 1, 0) = ::qifen::internal::add_up(err, ::std::abs(round_err));

	::qifen_matrix_set_kv(dest->a, ::std::move(new_a));
	::qifen_matrix_set_kv(dest->A, ::std::move(new_A));

	dest->is_quad_zero = false;
	dest->has_quad_minmax = false;
}

qifen_error_t qifen_qi_kv_div(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b, const qifen_qi_inv_config_t config)
{
	::qifen_qi_t tmp;
	::qifen_qi_ptr p = dest;

	if (dest == a) {
		::qifen_qi_init(ctx, tmp);
		p = tmp;
	}

	auto r = ::qifen_qi_kv_inv(ctx, p, b, config);
	if (r != ::qifen_error_succeeded) {
		if (dest == a) {
			::qifen_qi_clear(ctx, tmp);
		}
		return r;
	}
	::qifen_qi_kv_mul(ctx, dest, a, p);
	if (dest == a) {
		::qifen_qi_clear(ctx, tmp);
	}
	return ::qifen_error_succeeded;
}

qifen_error_t qifen_qi_kv_inv(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, const qifen_qi_inv_config_t config)
{
	if (config == nullptr) {
		config = ::qifen_qi_inv_config_default();
	}

	size_t a_size_old;

	::qifen_matrix_get_size(a->a, &a_size_old, nullptr);

	++ctx->num_dummy;

	::qifen_matrix_resize(a->a, ctx->num_dummy, 1);
	::qifen_matrix_resize(a->A, ctx->num_dummy, ctx->num_dummy);
	::pad_zero(a, a_size_old, ctx->num_dummy);

	::kv::interval<double> lin_range, quad_range;
	::std::tie(lin_range.lower(), lin_range.upper()) = ::qifen::internal::verify_linpart_range(ctx, a->a);
	::std::tie(quad_range.lower(), quad_range.upper()) = ::qifen::internal::verify_qi_quadpart_range(ctx, a);

	QIFEN_VAR_KV_MATRIX(a_a, a->a);
	QIFEN_VAR_KV_MATRIX(a_A, a->A);

	auto approx_range = a->x0 + lin_range + quad_range;
	bool is_negative = approx_range.upper() < 0.0;

	if (rad(approx_range) == 0.0) {
		::qifen_matrix_resize(dest->a, ctx->num_dummy, 1);
		::qifen_matrix_resize(dest->A, ctx->num_dummy, ctx->num_dummy);
		::pad_zero(dest, 0, ctx->num_dummy);

		auto r = 1.0 / ::kv::interval<double>(a->x0);

		dest->x0 = mid(r);
		::qifen_matrix_set_entry(dest->a, ctx->num_dummy - 1, 0, rad(r));

		return ::qifen_error_succeeded;
	}

	::boost::numeric::ublas::matrix<double> new_a(ctx->num_dummy, 1), new_A(ctx->num_dummy, ctx->num_dummy);

	auto on_error = [&]() {
		--ctx->num_dummy;

		::qifen_matrix_resize(a->a, ctx->num_dummy, 1);
		::qifen_matrix_resize(a->A, ctx->num_dummy, ctx->num_dummy);
	};

	if (zero_in(approx_range)) {
		return ::qifen_error_domain;
	}
	if (is_negative) {
		approx_range = -approx_range;
		a_a = -a_a;
		a_A = -a_A;
	}

	double r, p, q;
	::boost::numeric::ublas::matrix<double> m(3, 3);
	::boost::numeric::ublas::vector<double> b(3);
	::boost::numeric::ublas::permutation_matrix<> pm(3);

	bool chebyshev_failed = false, remez_failed = false;

	if (config->approx_method == ::qifen_approx_chebyshev_2) {
		double md = mid(approx_range);
		double rd = rad(approx_range);

		double T[3][3] = {
			{ 1.0, 0.0, 0.0 },
			{ -md / rd, 1.0 / rd, 0.0 },
			{ 2.0 * md * md / (rd * rd) - 1.0, -4.0 * md / (rd * rd), 2.0 / (rd * rd) },
		};
		double c[3] = {};
		double f[3];

		auto fn = [&](double x) {
			return 1.0 / x;
		};

		for (size_t i = 0; i < 3; ++i) {
			auto y = ::std::cos((2.0*i + 1) * ::pi / 6.0);
			f[i] = fn(y * rd + md);
		}

		for (size_t i = 0; i < 3; ++i) {
			for (size_t j = 0; j < 3; ++j) {
				c[i] += (::std::cos((::pi * static_cast<double>(i) * (j + 0.5)) / 3.0)) * f[j];
			}
			c[i] *= 2.0 / 3.0;
			if (i == 0)
				c[i] /= 2.0;
		}

		r = 0.0;
		p = 0.0;
		q = 0.0;

		for (size_t i = 0; i < 3; ++i) {
			r += T[i][2] * c[i];
			p += T[i][1] * c[i];
			q += T[i][0] * c[i];
		}
	}

	if (config->approx_method == ::qifen_approx_best_effort
		|| config->approx_method == ::qifen_approx_remez
		|| config->approx_method == ::qifen_approx_chebyshev) {
		// Chebyshev interpolation
		for (size_t i = 0; i < 3; ++i) {
			auto x = ::std::cos((2.0*i + 1) * ::pi / 6.0) * rad(approx_range) + mid(approx_range);
			m(i, 0) = x * x;
			m(i, 1) = x;
			m(i, 2) = 1.0;
			b(i) = 1.0 / x;
		}

		try {
			lu_factorize(m, pm);
			lu_substitute(m, pm, b);
		} catch (::std::exception &) {
			chebyshev_failed = true;
		}

		r = b(0);
		p = b(1);
		q = b(2);

		if (r <= 0.0 || p >= 0.0 || q <= 0.0) {
			chebyshev_failed = true;
		}
	}

	if ((config->approx_method == ::qifen_approx_best_effort || config->approx_method == ::qifen_approx_remez)
		&& !chebyshev_failed
		) {
		// Remez algorithm

		m.resize(4, 4, false);
		b.resize(4, false);
		pm.resize(4, false);

		double ra = approx_range.lower();
		double rb = approx_range.upper();
		double phi = ::inv::calc_phi(r, p);
		double x1 = ::inv::calc_x(phi, p, 0);
		double x2 = ::inv::calc_x(phi, p, 1);
		double e_last = 0.0;

		if (!in(x2, approx_range)) {
			x2 = ::inv::calc_x(phi, p, 2);
		}
		if (x1 > x2) {
			::std::swap(x1, x2);
		}

		for (size_t i = 0; i < config->num_iter; ++i) {
			m(0, 0) = ra * ra;
			m(0, 1) = ra;
			m(0, 2) = 1.0;
			m(0, 3) = 1.0;
			b(0) = 1.0 / ra;

			m(1, 0) = x1 * x1;
			m(1, 1) = x1;
			m(1, 2) = 1.0;
			m(1, 3) = -1.0;
			b(1) = 1.0 / x1;

			m(2, 0) = x2 * x2;
			m(2, 1) = x2;
			m(2, 2) = 1.0;
			m(2, 3) = 1.0;
			b(2) = 1.0 / x2;

			m(3, 0) = rb * rb;
			m(3, 1) = rb;
			m(3, 2) = 1.0;
			m(3, 3) = -1.0;
			b(3) = 1.0 / rb;

			try {
				pm = ::boost::numeric::ublas::permutation_matrix<>(4);
				lu_factorize(m, pm);
				lu_substitute(m, pm, b);
			} catch (std::exception &e) {
				remez_failed = (i == 0);
				break;
			}

			r = b(0);
			p = b(1);
			q = b(2);

			if (r <= 0.0 || p >= 0.0 || q <= 0.0) {
				remez_failed = true;
			}

			phi = ::inv::calc_phi(r, p);
			double x1_ = ::inv::calc_x(phi, p, 0);
			double x2_ = ::inv::calc_x(phi, p, 1);
			if (!in(x2_, approx_range)) {
				x2_ = ::inv::calc_x(phi, p, 2);
			}
			if (x1_ > x2_) {
				::std::swap(x1_, x2_);
			}

#if 1
			// multipoint exchange
			double error = ::std::abs(::inv::quad_approx(r, p, q, ra) - 1.0 / ra);

			error = ::std::max(error, ::std::abs(::inv::quad_approx(r, p, q, x1_) - 1.0 / x1_));
			error = ::std::max(error, ::std::abs(::inv::quad_approx(r, p, q, x2_) - 1.0 / x2_));
			error = ::std::max(error, ::std::abs(::inv::quad_approx(r, p, q, rb) - 1.0 / rb));

			if (::std::abs(e_last - error) < config->tolerance) {
				x1 = x1_;
				x2 = x2_;
				break;
			}

			x1 = x1_;
			x2 = x2_;
#else
			// single point exchange
			if (::std::abs(::inv::quad_approx(r, p, q, x1_) - 1.0 / x1_)
				> ::std::abs(::inv::quad_approx(r, p, q, x2_) - 1.0 / x2_)
				) {
				if (::std::abs(x1 - x1_) <= config->torelance) {
					x1 = x1_;
					break;
				}
				x1 = x1_;
			} else {
				if (::std::abs(x2 - x2_) <= config->torelance) {
					x2 = x2_;
					break;
				}
				x2 = x2_;
			}
#endif
		}
	}

	if (config->approx_method == ::qifen_approx_fast ||
		(config->approx_method == ::qifen_approx_best_effort && chebyshev_failed)) {

		::qifen::internal::round_to_up([&]() {
			auto t = a->x0 * a->x0;
			r = 1.0 / (t * a->x0);
			p = -3.0 / t;
			q = 3.0 / a->x0;
		});
	}

	if ((config->approx_method == ::qifen_approx_remez && remez_failed)
		|| (config->approx_method == ::qifen_approx_chebyshev && chebyshev_failed)
		) {
		// error
		on_error();
		return ::qifen_error_approx_failed;
	}

	if (config->approx_method == ::qifen_approx_linear) {
		// linear approximation
		// p * x + q

		::kv::interval<double> lower(approx_range.lower()), upper(approx_range.upper());
		::kv::interval<double> p(-1.0 / (lower * upper));
		::kv::interval<double> q(-p * pow(sqrt(lower) + sqrt(upper), 2) * 0.5);
		::kv::interval<double> delta(-p * pow(sqrt(lower) - sqrt(upper), 2) * 0.5);
		::kv::interval<double> tmp;
		double a_err = 0.0;
		double A_err_max = 0.0;

		// linear terms and quadratic terms
		::std::vector<double> gershgorin(ctx->num_dummy - 1);

		for (size_t i = 0; i < ctx->num_dummy - 1; ++i) {
			tmp = p * a_a(i, 0);
			new_a(i, 0) = mid(tmp);
			a_err = ::qifen::internal::add_up(a_err, rad(tmp));

			for (size_t j = i; j < ctx->num_dummy - 1; ++j) {
				tmp = p * a_A(i, j);
				new_A(i, j) = mid(tmp);
				gershgorin[i] = ::qifen::internal::add_up(gershgorin[i], rad(tmp));

				if (i != j) {
					new_A(j, i) = mid(tmp);
					gershgorin[j] = ::qifen::internal::add_up(gershgorin[j], rad(tmp));
				}
			}
		}

		A_err_max = *::std::max_element(gershgorin.begin(), gershgorin.end());
		A_err_max = ::qifen::internal::mul_up(A_err_max, static_cast<double>(ctx->num_dummy));

		// constant term
		tmp = p * a->x0 + q;
		dest->x0 = mid(tmp);

		// error term
		double err = ::qifen::internal::add_up(rad(tmp), ::qifen::internal::add_up(a_err, A_err_max));

		err = mag(::qifen::internal::add_up(err, rad(p * a->delta)) + delta);
		new_a(ctx->num_dummy - 1, 0) = err;

		for (size_t i = 0; i < ctx->num_dummy; ++i) {
			new_A(i, ctx->num_dummy - 1) = 0.0;
			new_A(ctx->num_dummy - 1, i) = 0.0;
		}

		::qifen_matrix_set_kv(dest->a, ::std::move(new_a));
		::qifen_matrix_set_kv(dest->A, ::std::move(new_A));
	} else {
		// quadratic approximation
		// r * x^2 + p * x + q

		// ::std::cout << r << ' ' << p << ' ' << q << ::std::endl;

		::qifen_matrix_resize(dest->a, ctx->num_dummy, 1);
		::qifen_matrix_resize(dest->A, ctx->num_dummy, ctx->num_dummy);

		double approx_err = 0.0;

		::kv::interval<double> ir(r), ip(p), iq(q);

		if (config->approx_method == ::qifen_approx_fast) {
			for (auto &i : { approx_range.lower(), approx_range.upper(), a->x0 }) {
				::kv::interval<double> x(i);
				approx_err = ::std::max(approx_err, mag(::inv::quad_approx(ir, ip, iq, x) - 1.0 / x));
			}

			// std::cout << approx_err << ' ' << rad(::inv::quad_approx(ir, ip, iq, ::kv::interval<double>(approx_range.lower())) - 1.0 / ::kv::interval<double>(approx_range.lower())) << std::endl;
		} else {
			try {
				auto phi = ::inv::calc_phi(ir, ip);
				auto x1 = ::inv::calc_x(phi, ip, 0), x2 = ::inv::calc_x(phi, ip, 1);

				if (!overlap(x2, approx_range))
					x2 = ::inv::calc_x(phi, ::kv::interval<double>(p), 2);

				// std::cout << x1 << ' ' << x2 << std::endl;

				if (!overlap(x1, approx_range) || !overlap(x2, approx_range)) {
					throw ::std::runtime_error("approximation is invalid");
				}

				approx_err = mag(::inv::quad_approx(ir, ip, iq, x1) - 1.0 / x1);

				for (auto &i : { x1, x2, ::kv::interval<double>(approx_range.lower()), ::kv::interval<double>(approx_range.upper()) }) {
					approx_err = ::std::max(approx_err, mag(::inv::quad_approx(ir, ip, iq, i) - 1.0 / i));
				}

				// std::cout << approx_err << ' ' << rad(::inv::quad_approx(ir, ip, iq, x1) - 1.0 / x1) << ' ' << rad(sqrt(-(pow(ir, 2) + pow(ip / 3.0, 3)))) << ' ' << mid((-(pow(ir, 2) + pow(ip / 3.0, 3)))) << ' ' << rad(phi) << ' ' << r << std::endl;
			} catch (::std::exception &e) {
				on_error();
				return ::qifen_error_approx_failed;
			}
		}

		::kv::interval<double> tmp, x0(a->x0);
		double err = approx_err;

		tmp = r * pow(x0, 2) + p * x0 + q;
		dest->x0 = mid(tmp);
		err = ::qifen::internal::add_up(err, rad(tmp));

		// linear terms
		tmp = 2.0 * x0 * r + p;
		for (size_t i = 0; i < ctx->num_dummy - 1; ++i) {
			auto v = tmp * a_a(i, 0);
			new_a(i, 0) = mid(v);
			err = ::qifen::internal::add_up(err, rad(v));
		}

		// quadratic terms

		double quad_err = 0.0;

		{
			::boost::numeric::ublas::matrix<::kv::interval<double>> A, tmp;
			::boost::numeric::ublas::matrix<::kv::interval<double>> e(ctx->num_dummy, 1, ::kv::interval<double>(-1.0, 1.0));
			::boost::numeric::ublas::matrix<::kv::interval<double>> ee(ctx->num_dummy, ctx->num_dummy, ::kv::interval<double>(-1.0, 1.0));

			for (size_t i = 0; i < ctx->num_dummy; ++i) {
				ee(i, i).lower() = 0.0;
			}

			tmp = prod(a_A, ee);
			A = prod(tmp, a_A);
			tmp = prod(a_A, e);
			A += ::kv::interval<double>(2.0) * prod(tmp, trans(a_a));
			tmp = a_a;
			A += prod(tmp, trans(tmp));
			A *= ir;
			tmp = a_A;
			A += (2.0 * ir * x0 + ip) * tmp;

			A = (A + trans(A)) * ::kv::interval<double>(0.5);

			::boost::numeric::ublas::matrix<double> m(ctx->num_dummy, ctx->num_dummy);

			for (size_t i = 0; i < ctx->num_dummy; ++i) {
				for (size_t j = 0; j < ctx->num_dummy; ++j) {
					double a, b;
					midrad(A(i, j), a, b);
					new_A(i, j) = a;
					m(i, j) = b;
				}
			}

			::qifen::internal::round_to_up([&]() {
				for (size_t i = 0; i < ctx->num_dummy; ++i) {
					quad_err += sum(row(m, i));
				}
				err += quad_err;
			});

			for (size_t i = 0; i < ctx->num_dummy; ++i) {
				new_A(i, ctx->num_dummy - 1) = 0.0;
				new_A(ctx->num_dummy - 1, i) = 0.0;
			}
		}

		// error term
		tmp = (approx_range * r + p) * a->delta;
		tmp += err;

		new_a(ctx->num_dummy - 1, 0) = tmp.upper();

		::qifen_matrix_set_kv(dest->a, ::std::move(new_a));
		::qifen_matrix_set_kv(dest->A, ::std::move(new_A));

		dest->delta = 0;
		dest->is_quad_zero = false;
		dest->has_quad_minmax = false;
	}

	if (is_negative) {
		::qifen_qi_negate(ctx, dest, dest);
	}

	return ::qifen_error_succeeded;
}

#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
void qifen_qi_intlab_add_scalar(qifen_qi_context_t ctx, qifen_qi_t dest, const qifen_qi_t a, double b)
{
	::qifen_qi_add_scalar(ctx, dest, a, b);
}
#endif
