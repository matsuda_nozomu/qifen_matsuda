#include <memory>

#include <qifen.h>
#include <qifen/mex-compatible.h>
#include <qifen/interval-internal.hpp>
#include <qifen/internal.hpp>

#include <kv/rdouble.hpp>

using interval_t = qifen_interval_struct::interval_t;

const size_t qifen_sizeof_interval = sizeof(qifen_interval_t);

namespace qifen {
	namespace internal {
#if !defined(QIFEN_CONFIG_DISABLE_KV)
		::kv::interval<double> interval_to_kv(const ::qifen_interval_t v)
		{
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
			return **v;
#  elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
			::kv::interval<double> r;
			::qifen_interval_get_infsup(v, &r.lower(), &r.upper());
			return r;
#  endif
		}
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
		::mxArray *interval_to_intlab(const ::qifen_interval_t v)
		{
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
			::mxArray *inf = ::mxCreateDoubleScalar((*v)->lower()), *sup = ::mxCreateDoubleScalar((*v)->upper());
			::mxArray *lhs;
			::mxArray *rhs[] = { inf, sup };
			::mexCallMATLAB(1, &lhs, 2, rhs, "infsup");
			::mxDestroyArray(inf);
			::mxDestroyArray(sup);
			return lhs;
#  elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
			return const_cast<::mxArray*>(v->value);
#  endif
		}
#endif

#if !defined(QIFEN_CONFIG_DISABLE_KV)
		void interval_set(::qifen_interval_t dest, const ::kv::interval<double> &v)
		{
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
			**dest = v;
#  elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
			::qifen_interval_set_infsup(dest, v.lower(), v.upper());
#  endif
		}
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
		void interval_set(::qifen_interval_t dest, ::mxArray *v)
		{
			::mxArray *pinf = ::mxGetField(v, 0, "inf");
			::mxArray *psup = ::mxGetField(v, 0, "sup");
			::qifen_interval_set_infsup(dest, ::mxGetScalar(pinf), ::mxGetScalar(psup));
		}
#endif
	}
}

void qifen_interval_init(qifen_interval_t v)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	new(&v->storage) interval_t;
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::mxArray *rhs = ::mxCreateDoubleScalar(0.0);
	::mexCallMATLAB(1, &v->value, 1, &rhs, "intval");
	::mxDestroyArray(rhs);
#endif
}

void qifen_interval_init_infsup(qifen_interval_t v, double inf, double sup)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	new(&v->storage) interval_t(inf, sup);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::mxArray *pinf = ::mxCreateDoubleScalar(inf), *psup = ::mxCreateDoubleScalar(sup);
	::mxArray *rhs[] = { pinf, psup };
	::mexCallMATLAB(1, &v->value, 2, rhs, "infsup");
	::mxDestroyArray(pinf);
	::mxDestroyArray(psup);
#endif
}

void qifen_interval_clear(qifen_interval_t v)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	(*v)->~interval_t();
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
#endif
}

void qifen_interval_set(qifen_interval_t dest, const qifen_interval_t v)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen::internal::interval_set(dest, **v);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen::internal::interval_set(dest, v->value);
#endif
}

void qifen_interval_set_infsup(qifen_interval_t dest, double inf, double sup)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	(*dest)->lower() = inf;
	(*dest)->upper() = sup;
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::mxArray *pinf = ::mxGetField(dest->value, 0, "inf");
	::mxArray *psup = ::mxGetField(dest->value, 0, "sup");
	*::mxGetPr(pinf) = inf;
	*::mxGetPr(psup) = sup;
#endif
}

void qifen_interval_set_infsup_string(qifen_interval_t dest, const char *inf, const char *sup)
{
	double i, s;

	if (inf != nullptr) {
		::qifen::internal::range<const char> range = { inf, ::std::strchr(inf, '\0') };
		i = ::qifen::internal::stod(range, nullptr, -1);

		if (sup == nullptr) {
			s = ::qifen::internal::stod(range, nullptr, 1);
		} else {
			range = { sup, ::std::strchr(sup, '\0') };
			s = ::qifen::internal::stod(range, nullptr, 1);
		}
	} else if (sup != nullptr) {
		::qifen::internal::range<const char> range = { sup, ::std::strchr(sup, '\0') };
		i = ::qifen::internal::stod(range, nullptr, -1);
		s = ::qifen::internal::stod(range, nullptr, 1);
	} else {
		i = 0.0;
		s = 0.0;
	}

#if defined(QIFEN_CONFIG_INTERVAL_KV)
	(*dest)->lower() = i;
	(*dest)->upper() = s;
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::mxArray *pinf = ::mxGetField(dest->value, 0, "inf");
	::mxArray *psup = ::mxGetField(dest->value, 0, "sup");
	*::mxGetPr(pinf) = i;
	*::mxGetPr(psup) = s;
#endif
}

void qifen_interval_get_infsup(const qifen_interval_t v, double *inf, double *sup)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	if (inf != nullptr)
		*inf = (*v)->lower();
	if (sup != nullptr)
		*sup = (*v)->upper();
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	if (inf != nullptr) {
		::mxArray *pinf = ::mxGetField(v->value, 0, "inf");
		*inf = ::mxGetScalar(pinf);
	}
	if (sup != nullptr) {
		::mxArray *psup = ::mxGetField(v->value, 0, "sup");
		*sup = ::mxGetScalar(psup);
	}
#endif
}

void qifen_interval_get_midrad(const qifen_interval_t v, double *pmid, double *prad)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	if (pmid != nullptr && prad != nullptr) {
		midrad(**v, *pmid, *prad);
	} else if (pmid != nullptr) {
		*pmid = mid(**v);
	} else if (prad != nullptr) {
		*prad = rad(**v);
	}
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	if (pmid != nullptr) {
		::mxArray *lhs;
		::mexCallMATLAB(1, &lhs, 1, const_cast<::mxArray**>(&v->value), "mid");
		*pmid = ::mxGetScalar(lhs);
		::mxDestroyArray(lhs);
	}
	if (prad != nullptr) {
		::mxArray *lhs;
		::mexCallMATLAB(1, &lhs, 1, const_cast<::mxArray**>(&v->value), "rad");
		*prad = ::mxGetScalar(lhs);
		::mxDestroyArray(lhs);
	}
#endif
}

void qifen_interval_negate(qifen_interval_t dest, const qifen_interval_t a)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	**dest = -**a;
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	QIFEN_MEX_LHS_AS_DEST_INTERVAL(lhs, dest);
	::mexCallMATLAB(1, &lhs, 1, const_cast<::mxArray**>(&a->value), "uminus");
#endif
}

void qifen_interval_add(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_interval_kv_add(dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_interval_intlab_add(dest, a, b);
#endif
}

void qifen_interval_sub(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_interval_kv_sub(dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_interval_intlab_sub(dest, a, b);
#endif
}

void qifen_interval_mul(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_interval_kv_mul(dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_interval_intlab_mul(dest, a, b);
#endif
}

void qifen_interval_div(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_interval_kv_div(dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_interval_intlab_div(dest, a, b);
#endif
}

void qifen_interval_mul_scalar(qifen_interval_t dest, const qifen_interval_t a, double b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_interval_kv_mul_scalar(dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_interval_intlab_mul_scalar(dest, a, b);
#endif
}

void qifen_interval_sin(qifen_interval_t dest, const qifen_interval_t a)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_interval_kv_sin(dest, a);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_interval_intlab_sin(dest, a);
#endif
}

void qifen_interval_cos(qifen_interval_t dest, const qifen_interval_t a)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_interval_kv_cos(dest, a);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_interval_intlab_cos(dest, a);
#endif
}

void qifen_interval_pi(qifen_interval_t dest)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_interval_kv_pi(dest);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_interval_intlab_pi(dest);
#endif
}

#if !defined(QIFEN_CONFIG_DISABLE_KV)
void qifen_interval_init_kv(qifen_interval_t dest, const kv::interval<double> &a)
{
	qifen_interval_init_infsup(dest, a.lower(), a.upper());
}

void qifen_interval_kv_negate(qifen_interval_t dest, const qifen_interval_t a)
{
	::qifen_interval_negate(dest, a);
}

void qifen_interval_kv_add(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b)
{
	QIFEN_VAR_KV_INTERVAL(aa, a);
	QIFEN_VAR_KV_INTERVAL(bb, b);

	::qifen::internal::interval_set(dest, aa + bb);
}

void qifen_interval_kv_sub(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b)
{
	QIFEN_VAR_KV_INTERVAL(aa, a);
	QIFEN_VAR_KV_INTERVAL(bb, b);

	::qifen::internal::interval_set(dest, aa - bb);
}

void qifen_interval_kv_mul(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b)
{
	QIFEN_VAR_KV_INTERVAL(aa, a);
	QIFEN_VAR_KV_INTERVAL(bb, b);

	::qifen::internal::interval_set(dest, aa * bb);
}

void qifen_interval_kv_div(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b)
{
	QIFEN_VAR_KV_INTERVAL(aa, a);
	QIFEN_VAR_KV_INTERVAL(bb, b);

	::qifen::internal::interval_set(dest, aa / bb);
}

void qifen_interval_kv_mul_scalar(qifen_interval_t dest, const qifen_interval_t a, double b)
{
	QIFEN_VAR_KV_INTERVAL(aa, a);

	::qifen::internal::interval_set(dest, aa * b);
}

void qifen_interval_kv_sin(qifen_interval_t dest, const qifen_interval_t a)
{
	QIFEN_VAR_KV_INTERVAL(aa, a);

	::qifen::internal::interval_set(dest, sin(aa));
}

void qifen_interval_kv_cos(qifen_interval_t dest, const qifen_interval_t a)
{
	QIFEN_VAR_KV_INTERVAL(aa, a);

	::qifen::internal::interval_set(dest, cos(aa));
}

void qifen_interval_kv_pi(qifen_interval_t dest)
{
	::qifen::internal::interval_set(dest, kv::constants<kv::interval<double>>::pi());
}

void qifen_interval_to_kv(const qifen_interval_t v, ::kv::interval<double> &dest)
{
	dest = ::qifen::internal::interval_to_kv(v);
}
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
int qifen_interval_init_intlab(qifen_interval_t dest, const mxArray *a)
{
	QIFEN_ENSURE_TYPE("intval", a);
	QIFEN_ENSURE_SCALAR(a);

	const mxArray *pinf = mxGetField(a, 0, "inf");
	const mxArray *psup = mxGetField(a, 0, "sup");

	qifen_interval_init_infsup(dest, ::mxGetScalar(pinf), ::mxGetScalar(psup));

	return 1;
}

void qifen_interval_intlab_negate(qifen_interval_t dest, const qifen_interval_t a)
{
	::qifen_interval_negate(dest, a);
}

void qifen_interval_intlab_add(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b)
{
	QIFEN_VAR_INTLAB_INTERVAL(aa, a);
	QIFEN_VAR_INTLAB_INTERVAL(bb, b);
	QIFEN_MEX_LHS_AS_DEST_INTERVAL(lhs, dest);

	::mxArray *rhs[] = { aa, bb };
	::mexCallMATLAB(1, &lhs, 2, rhs, "plus");
}

void qifen_interval_intlab_sub(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b)
{
	QIFEN_VAR_INTLAB_INTERVAL(aa, a);
	QIFEN_VAR_INTLAB_INTERVAL(bb, b);
	QIFEN_MEX_LHS_AS_DEST_INTERVAL(lhs, dest);

	::mxArray *rhs[] = { aa, bb };
	::mexCallMATLAB(1, &lhs, 2, rhs, "minus");
}

void qifen_interval_intlab_mul(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b)
{
	QIFEN_VAR_INTLAB_INTERVAL(aa, a);
	QIFEN_VAR_INTLAB_INTERVAL(bb, b);
	QIFEN_MEX_LHS_AS_DEST_INTERVAL(lhs, dest);

	::mxArray *rhs[] = { aa, bb };
	::mexCallMATLAB(1, &lhs, 2, rhs, "times");
}

void qifen_interval_intlab_div(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b)
{
	QIFEN_VAR_INTLAB_INTERVAL(aa, a);
	QIFEN_VAR_INTLAB_INTERVAL(bb, b);
	QIFEN_MEX_LHS_AS_DEST_INTERVAL(lhs, dest);

	::mxArray *rhs[] = { aa, bb };
	::mexCallMATLAB(1, &lhs, 2, rhs, "rdivide");
}

void qifen_interval_intlab_mul_scalar(qifen_interval_t dest, const qifen_interval_t a, double b)
{
	QIFEN_VAR_INTLAB_INTERVAL(aa, a);
	QIFEN_MEX_LHS_AS_DEST_INTERVAL(lhs, dest);

	::mxArray *rhs[] = { aa, ::mxCreateDoubleScalar(b) };
	::mexCallMATLAB(1, &lhs, 2, rhs, "times");
	::mxDestroyArray(rhs[1]);
}

void qifen_interval_intlab_sin(qifen_interval_t dest, const qifen_interval_t a)
{
	QIFEN_VAR_INTLAB_INTERVAL(aa, a);
	QIFEN_MEX_LHS_AS_DEST_INTERVAL(lhs, dest);

	::mexCallMATLAB(1, &lhs, 1, &aa, "sin");
}

void qifen_interval_intlab_cos(qifen_interval_t dest, const qifen_interval_t a)
{
	QIFEN_VAR_INTLAB_INTERVAL(aa, a);
	QIFEN_MEX_LHS_AS_DEST_INTERVAL(lhs, dest);

	::mexCallMATLAB(1, &lhs, 1, &aa, "cos");
}

void qifen_interval_intlab_pi(qifen_interval_t dest)
{
	QIFEN_MEX_LHS_AS_DEST_INTERVAL(lhs, dest);
	::mxArray *rhs = ::mxCreateString("pi");
	::mexCallMATLAB(1, &lhs, 1, &rhs, "intval");
	::mxDestroyArray(rhs);
}

void qifen_interval_to_intlab(const qifen_interval_t v, mxArray **dest)
{
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
	*dest = ::qifen::internal::interval_to_intlab(v);
#  else
	::qifen_interval_t r;
	::qifen_interval_init(r);
	::qifen_interval_set(r, v);
	*dest = r->value;
#  endif
}
#endif
