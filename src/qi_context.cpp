#include <qifen/qi_context.h>

qifen_qi_context_ptr qifen_qi_context_new()
{
	::qifen_qi_context_ptr p = new ::qifen_qi_context_struct;
	::qifen_qi_context_init(p);
	return p;
}

void qifen_qi_context_delete(qifen_qi_context_t v)
{
	::qifen_qi_context_clear(v);
	delete v;
}

void qifen_qi_context_init(qifen_qi_context_t v)
{
	v->num_dummy = 0;
}

void qifen_qi_context_clear(qifen_qi_context_t v)
{
}

void qifen_qi_context_set_num_dummy(qifen_qi_context_t v, size_t n)
{
	v->num_dummy = n;
}
