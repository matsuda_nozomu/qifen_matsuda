#define QIFEN_CONFIG_INTERVAL_KV
#define QIFEN_CONFIG_DISABLE_INTLAB

// #define AFFINE_MULT 2

#include <qifen.h>
#include <iostream>
#include <boost/numeric/ublas/io.hpp>
#include <boost/scope_exit.hpp>
#include <kv/affine.hpp>
#include <kv/rdouble.hpp>
#include <cstdlib>
#include <chrono>

#include <qifen/internal.hpp>

template <typename F>
::std::chrono::nanoseconds time(F f)
{
	using namespace ::std::chrono;

	high_resolution_clock::time_point begin, end;

	begin = high_resolution_clock::now();
	f();
	end = high_resolution_clock::now();

	return duration_cast<nanoseconds>(end - begin);
}

void calc_qifen(double in_radius, double out_radius, size_t nmax, const ::qifen_qi_inv_config_struct &config)
{
	::std::chrono::nanoseconds t{}, t2;

	for (size_t n = 1; n <= nmax; ++n) {
		double inf = -1.0, sup = -1.0;

		t2 = t;

		t = time([&]() {
			::qifen_qi_context_t ctx;
			::qifen_qi_t x, f, t1, t2, t3;

			::qifen_qi_context_init(ctx);
			::qifen_qi_init(ctx, x);
			::qifen_qi_init(ctx, f);
			::qifen_qi_init(ctx, t1);
			::qifen_qi_init(ctx, t2);
			::qifen_qi_init(ctx, t3);

			for (size_t i = 0; i < n; ++i) {
				double a = -in_radius + 2.0 * in_radius * i / n;
				double b = -in_radius + 2.0 * in_radius * (i + 1) / n;

				::qifen_qi_set_infsup(ctx, x, 1e4 + a, 1e4 + b);

				// t1 = x + 1.0
				::qifen_qi_add_scalar(ctx, t1, x, 1.0);
				// t2 = 1.0 / x
				::qifen_qi_inv(ctx, t2, x, config);
				// t3 = 1.0 / (x+1.0)
				::qifen_qi_inv(ctx, t3, t1, config);
				// t1 = x * (x + 1.0)
				::qifen_qi_mul(ctx, t1, x, t1);
				// t2 = (1.0 / x) - (1.0 / (x+1.0))
				::qifen_qi_sub(ctx, t2, t2, t3);
				::qifen_qi_mul(ctx, x, t1, t2);

				// t1 = x * x
				::qifen_qi_mul(ctx, t1, x, x);
				// t2 = 2.0 * x
				::qifen_qi_mul_scalar(ctx, t2, x, 2.0);
				// f = x*x - 2.0*x
				::qifen_qi_sub(ctx, f, t1, t2);

				auto r = ::qifen_qi_get_infsup(ctx, f);

				inf = ::std::min(inf, r.first);
				sup = ::std::max(sup, r.second);

				::qifen_qi_context_set_num_dummy(ctx, 0);
			}

			::qifen_qi_clear(ctx, x);
			::qifen_qi_clear(ctx, f);
			::qifen_qi_clear(ctx, t1);
			::qifen_qi_clear(ctx, t2);
			::qifen_qi_clear(ctx, t3);
			::qifen_qi_context_clear(ctx);
		});

		if ((sup - inf) / 2.0 <= out_radius) {
			::std::cout << "qifen_qi_t" << ::std::endl;
			::std::cout << "  n = " << n << ::std::endl;
			::std::cout << "  rad = " << (sup - inf) / 2.0 << ::std::endl;
			::std::cout << "  " << std::chrono::duration_cast<std::chrono::microseconds>(t2).count() << "us" << ::std::endl;
			::std::cout << "  " << std::chrono::duration_cast<std::chrono::microseconds>(t).count() << "us" << ::std::endl;
			break;
		}
	}
}

void calc_kv(double in_radius, double out_radius, size_t nmax)
{
	::std::chrono::nanoseconds t{}, t2;

	for (size_t n = 1; n <= nmax; ++n) {
		double inf = -1.0, sup = -1.0;

		t2 = t;

		t = time([&]() {
			::kv::affine<double> x, a2;

			for (size_t i = 0; i < n; ++i) {
				double a = -in_radius + 2.0 * in_radius * i / n;
				double b = -in_radius + 2.0 * in_radius * (i + 1) / n;

				x = ::kv::interval<double>(1e4 + a, 1e4 + b);

				a2 = x + 1.0;
				a2 = x * a2 * (1.0 / x - 1.0 / a2);
				a2 = pow(a2, 2) - 2.0 * a2;

				auto itv = to_interval(a2);

				inf = ::std::min(inf, itv.lower());
				sup = ::std::max(sup, itv.upper());

				::kv::affine<double>::maxnum() = 0;
			}
		});

		if ((sup - inf) / 2.0 <= out_radius) {
			::std::cout << "kv::affine<double>" << ::std::endl;
			::std::cout << "  n = " << n << ::std::endl;
			::std::cout << "  rad = " << (sup - inf) / 2.0 << ::std::endl;
			::std::cout << "  " << std::chrono::duration_cast<std::chrono::microseconds>(t2).count() << "us" << ::std::endl;
			::std::cout << "  " << std::chrono::duration_cast<std::chrono::microseconds>(t).count() << "us" << ::std::endl;
			break;
		}
	}
}

int main(int argc, char **argv)
{
	::std::cout.setf(::std::ios::scientific);
	::std::cout.precision(15);

    double in = 1e-1;
	double out = 5e-6;
	size_t nmax = 10000;

    if (argc > 1) {
        in = ::std::stod(argv[1]);
    }
	if (argc > 2) {
		out = ::std::stod(argv[2]);
	}
	if (argc > 3) {
		nmax = ::std::stoul(argv[3]);
	}

	for (size_t i = 0; i < 10; ++i)
		::calc_qifen(in, out, nmax,{ ::qifen_approx_fast });
	for (size_t i = 0; i < 10; ++i)
		::calc_kv(in, out, nmax);
}
