#define QIFEN_CONFIG_INTERVAL_KV
#define QIFEN_CONFIG_DISABLE_INTLAB

#define AFFINE_MULT 2

#include <qifen.h>
#include <iostream>
#include <boost/numeric/ublas/io.hpp>
#include <boost/scope_exit.hpp>
#include <kv/affine.hpp>
#include <kv/rdouble.hpp>
#include <cstdlib>
#include <chrono>

#include <qifen/internal.hpp>

template <typename F>
::std::chrono::nanoseconds time(F f)
{
	using namespace ::std::chrono;

	high_resolution_clock::time_point begin, end;

	begin = high_resolution_clock::now();
	f();
	end = high_resolution_clock::now();

	return duration_cast<nanoseconds>(end - begin);
}

#include <boost/numeric/ublas/io.hpp>

::std::pair<double, double> calc_qifen()
{
	::qifen_qi_context_t ctx;
	::qifen_qi_t x, y, z, f;

	::qifen_qi_context_init(ctx);
	::qifen_qi_init_infsup(ctx, x, 1.0, 2.0);
	::qifen_qi_init_infsup(ctx, y, 3.0, 4.0);
	::qifen_qi_init_infsup(ctx, z, 5.0, 6.0);
	::qifen_qi_init(ctx, f);

	BOOST_SCOPE_EXIT_ALL(&) {
		::qifen_qi_clear(ctx, x);
		::qifen_qi_clear(ctx, y);
		::qifen_qi_clear(ctx, z);
		::qifen_qi_clear(ctx, f);
		::qifen_qi_context_clear(ctx);
	};

	std::cout << x->x0 << std::endl;
	std::cout << **x->a << std::endl;
	std::cout << **x->A << std::endl;

	std::cout << y->x0 << std::endl;
	std::cout << **y->a << std::endl;
	std::cout << **y->A << std::endl;

	std::cout << z->x0 << std::endl;
	std::cout << **z->a << std::endl;
	std::cout << **z->A << std::endl;

	::qifen_qi_mul(ctx, f, x, y);

	std::cout << f->x0 << std::endl;
	std::cout << **f->a << std::endl;
	std::cout << **f->A << std::endl;

	::qifen_qi_inv(ctx, x, z, { ::qifen_verify_eig_auto, ::qifen_approx_fast });

	std::cout << ::qifen_qi_get_infsup(ctx, x).first << ' ';
	std::cout << ::qifen_qi_get_infsup(ctx, x).second << std::endl;

	::qifen_qi_div(ctx, f, f, z, { ::qifen_verify_eig_auto, ::qifen_approx_fast });

	std::cout << f->x0 << std::endl;
	std::cout << **f->a << std::endl;
	std::cout << **f->A << std::endl;

	return ::qifen_qi_get_infsup(ctx, f);
}

::std::pair<double, double> calc_kv()
{
	::kv::affine<double> x, y, z, f;

	x = ::kv::interval<double>(1.0, 2.0);
	y = ::kv::interval<double>(3.0, 4.0);
	z = ::kv::interval<double>(5.0, 6.0);
	f = x * y / z;

	auto i = to_interval(f);

	return ::std::make_pair(i.lower(), i.upper());
}

::std::pair<double, double> calc_qifen_2()
{
	::qifen_qi_context_t ctx;
	::qifen_qi_t x, y, f;

	::qifen_qi_context_init(ctx);
	::qifen_qi_init_infsup(ctx, x, 0.0, 10.0);
	::qifen_qi_init_infsup(ctx, y, 0.0, 0.25);
	::qifen_qi_init(ctx, f);

	BOOST_SCOPE_EXIT_ALL(&) {
		::qifen_qi_clear(ctx, x);
		::qifen_qi_clear(ctx, y);
		::qifen_qi_clear(ctx, f);
		::qifen_qi_context_clear(ctx);
	};

	::qifen_qi_mul(ctx, f, x, x);
	::qifen_qi_mul(ctx, y, y, y);
	::qifen_qi_add(ctx, f, f, y);

	return ::qifen_qi_get_infsup(ctx, f);
}

::std::pair<double, double> calc_kv_2()
{
	::kv::affine<double> x, y, f;

	x = ::kv::interval<double>(0.0, 10.0);
	y = ::kv::interval<double>(0.0, 0.25);
	f = pow(x, 2) + pow(y, 2);

	auto i = to_interval(f);

	return ::std::make_pair(i.lower(), i.upper());
}

int main()
{
	::std::cout.setf(::std::ios::scientific);
	::std::cout.precision(15);

	auto q = ::calc_qifen();
	::std::cout << q.first << ' ' << q.second << ' ' << q.second - q.first << ' ';

	return 0;

	::std::size_t a = 0;

	for (::std::size_t i = 0; i < 10000; ++i) {
		auto t = ::time([&]() {
			calc_qifen();
		});
		a += t.count();
	}

	::std::cout << ::std::chrono::duration_cast<::std::chrono::microseconds>(::std::chrono::nanoseconds(a)).count() / 10000.0 << ::std::endl;

	q = ::calc_kv();
	::std::cout << q.first << ' ' << q.second << ' ' << q.second - q.first << ' ';

	a = 0;

	for (::std::size_t i = 0; i < 10000; ++i) {
		auto t = ::time([&]() {
			::kv::affine<double>::maxnum() = 0;
			calc_kv();
		});
		a += t.count();
	}

	::std::cout << ::std::chrono::duration_cast<::std::chrono::microseconds>(::std::chrono::nanoseconds(a)).count() / 10000.0 << ::std::endl;

	q = ::calc_qifen_2();
	::std::cout << q.first << ' ' << q.second << ' ' << q.second - q.first << ' ';
	
	a = 0;

	for (::std::size_t i = 0; i < 10000; ++i) {
		auto t = ::time([&]() {
			calc_qifen_2();
		});
		a += t.count();
	}

	::std::cout << ::std::chrono::duration_cast<::std::chrono::microseconds>(::std::chrono::nanoseconds(a)).count() / 10000.0 << ::std::endl;

	q = ::calc_kv_2();
	::std::cout << q.first << ' ' << q.second << ' ' << q.second - q.first << ' ';

	a = 0;

	for (::std::size_t i = 0; i < 10000; ++i) {
		auto t = ::time([&]() {
			::kv::affine<double>::maxnum() = 0;
			calc_kv_2();
		});
		a += t.count();
	}

	::std::cout << ::std::chrono::duration_cast<::std::chrono::microseconds>(::std::chrono::nanoseconds(a)).count() / 10000.0 << ::std::endl;
}
