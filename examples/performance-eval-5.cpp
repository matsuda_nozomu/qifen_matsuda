#define QIFEN_CONFIG_INTERVAL_KV
#define QIFEN_CONFIG_DISABLE_INTLAB

#define AFFINE_MULT 2

#include <qifen.h>
#include <iostream>
#include <boost/numeric/ublas/io.hpp>
#include <boost/scope_exit.hpp>
#include <kv/affine.hpp>
#include <kv/rdouble.hpp>
#include <cstdlib>

#include <qifen/internal.hpp>

::std::pair<double, double> calc_qifen(double epsilon, double delta, const ::qifen_qi_inv_config_struct &config)
{
	//a2 = a * b * (b / a - a / b);
	//a2 = a2 - b * b + a * a;

	::qifen_qi_context_t ctx;
	::qifen_qi_t x, y, z, t1, t2, t3;

	::qifen_qi_context_init(ctx);
	::qifen_qi_init_infsup(ctx, x, 1e4 - epsilon, 1e4 + epsilon);
	::qifen_qi_init_infsup(ctx, y, 1e4 + delta - epsilon, 1e4 + delta + epsilon);
	::qifen_qi_init_infsup(ctx, z, 1e4 + 2.0 * delta - epsilon, 1e4 + 2.0 * delta + epsilon);
	::qifen_qi_init(ctx, t1);
	::qifen_qi_init(ctx, t2);
	::qifen_qi_init(ctx, t3);

	BOOST_SCOPE_EXIT_ALL(&) {
		::qifen_qi_clear(ctx, x);
		::qifen_qi_clear(ctx, y);
		::qifen_qi_clear(ctx, z);
		::qifen_qi_clear(ctx, t1);
		::qifen_qi_clear(ctx, t2);
		::qifen_qi_clear(ctx, t3);
		::qifen_qi_context_clear(ctx);
	};

	// t1 = y * z
	::qifen_qi_mul(ctx, t1, y, z);
	// t1 = y * z / x
	if (::qifen_qi_div(ctx, t1, t1, x, config) != ::qifen_error_succeeded)
		return ::std::make_pair(0.0, 0.0);
	// t2 = x / y
	if (::qifen_qi_div(ctx, t2, x, y, config) != ::qifen_error_succeeded)
		return ::std::make_pair(0.0, 0.0);
	// t3 = x * y
	::qifen_qi_mul(ctx, t3, x, y);
	// t3 = x * y * z
	::qifen_qi_mul(ctx, t3, t3, z);
	// t1 = y * z / x - x / y
	::qifen_qi_sub(ctx, t1, t1, t2);
	// t1 = x * y * z * (y * z / x - x / y)
	::qifen_qi_mul(ctx, t1, t3, t1);

	// t2 = y * y
	::qifen_qi_mul(ctx, t2, y, y);
	// t2 = y * y * z
	::qifen_qi_mul(ctx, t2, t2, z);
	// t2 = y * y * z * z
	::qifen_qi_mul(ctx, t2, t2, z);
	// t3 = x * x
	::qifen_qi_mul(ctx, t3, x, x);
	// t3 = x * x * z
	::qifen_qi_mul(ctx, t3, t3, z);
	// t1 = x * y * z * (y * z / x - x / y) - y * y * z * z
	::qifen_qi_sub(ctx, t1, t1, t2);
	// t1 = x * y * z * (y * z / x - x / y) - y * y * z * z + x * x * z
	::qifen_qi_add(ctx, t1, t1, t3);

	auto r = ::qifen_qi_get_infsup(ctx, t1);

	if (0.0 >= r.first && 0.0 <= r.second)
		return r;
	else
		return ::std::make_pair(1.0, 1.0);
}

int main(int argc, char **argv)
{
	::std::cout.setf(::std::ios::scientific);
	::std::cout.precision(15);

    double epsilon = 1e2;
	double delta = 0.0;

    if (argc > 1) {
        epsilon = ::std::stod(argv[1]);
    }
	if (argc > 2) {
		delta = ::std::stod(argv[2]);
	}

	auto q = ::calc_qifen(epsilon, delta, { ::qifen_approx_chebyshev });
	::std::cout << q.first << ' ' << q.second << ' ' << (q.second - q.first) / 2.0 << ' ';

	q = ::calc_qifen(epsilon, delta, { ::qifen_approx_chebyshev_2 });
	::std::cout << q.first << ' ' << q.second << ' ' << (q.second - q.first) / 2.0 << ' ';

	q = ::calc_qifen(epsilon, delta, { ::qifen_approx_remez, 10 });
	::std::cout << q.first << ' ' << q.second << ' ' << (q.second - q.first) / 2.0 << ' ';
	
	q = ::calc_qifen(epsilon, delta, { ::qifen_approx_fast });
	::std::cout << q.first << ' ' << q.second << ' ' << (q.second - q.first) / 2.0 << ' ';

	::kv::affine<double> a, b, c, a2;

	a = ::kv::interval<double>(1e4 - epsilon, 1e4 + epsilon);
	b = ::kv::interval<double>(1e4 + delta - epsilon, 1e4 + delta + epsilon);
	c = ::kv::interval<double>(1e4 + 2.0 * delta - epsilon, 1e4 + 2.0 * delta + epsilon);
	a2 = a * b * c * (b * c / a - a / b);
	a2 = a2 - pow(b, 2) * pow(c, 2) + pow(a, 2) * c;

	::std::cout << rad(to_interval(a2)) << ::std::endl;
	::std::cout << to_interval(a2) << ::std::endl;
}
