	#pragma once

#include <utility>

#include <qifen/config.h>
#include <qifen/types.h>

#if defined(__cplusplus)
extern "C" {
#endif

extern const size_t qifen_sizeof_interval;

void qifen_interval_init(qifen_interval_t v);
void qifen_interval_init_infsup(qifen_interval_t v, double inf, double sup);
void qifen_interval_clear(qifen_interval_t v);
void qifen_interval_set(qifen_interval_t dest, const qifen_interval_t v);
void qifen_interval_set_infsup(qifen_interval_t dest, double inf, double sup);
void qifen_interval_set_infsup_string(qifen_interval_t dest, const char *inf, const char *sup);
void qifen_interval_get_infsup(const qifen_interval_t v, double *inf, double *sup);
void qifen_interval_get_midrad(const qifen_interval_t v, double *mid, double *rad);
void qifen_interval_negate(qifen_interval_t dest, const qifen_interval_t a);
void qifen_interval_add(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b);
void qifen_interval_sub(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b);
void qifen_interval_mul(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b);
void qifen_interval_div(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b);
void qifen_interval_mul_scalar(qifen_interval_t dest, const qifen_interval_t a, double b);
void qifen_interval_sin(qifen_interval_t dest, const qifen_interval_t a);
void qifen_interval_cos(qifen_interval_t dest, const qifen_interval_t a);
void qifen_interval_pi(qifen_interval_t dest);

#if !defined(QIFEN_CONFIG_DISABLE_KV)
#  if defined(__cplusplus)
void qifen_interval_init_kv(qifen_interval_t dest, const kv::interval<double> &a);
#  endif
void qifen_interval_kv_negate(qifen_interval_t dest, const qifen_interval_t a);
void qifen_interval_kv_add(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b);
void qifen_interval_kv_sub(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b);
void qifen_interval_kv_mul(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b);
void qifen_interval_kv_div(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b);
void qifen_interval_kv_mul_scalar(qifen_interval_t dest, const qifen_interval_t a, double b);
void qifen_interval_kv_sin(qifen_interval_t dest, const qifen_interval_t a);
void qifen_interval_kv_cos(qifen_interval_t dest, const qifen_interval_t a);
void qifen_interval_kv_pi(qifen_interval_t dest);
#  if defined(__cplusplus)
void qifen_interval_to_kv(const qifen_interval_t v, ::kv::interval<double> &dest);
#  endif
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
int qifen_interval_init_intlab(qifen_interval_t dest, const mxArray *a);
void qifen_interval_intlab_negate(qifen_interval_t dest, const qifen_interval_t a);
void qifen_interval_intlab_add(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b);
void qifen_interval_intlab_sub(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b);
void qifen_interval_intlab_mul(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b);
void qifen_interval_intlab_div(qifen_interval_t dest, const qifen_interval_t a, const qifen_interval_t b);
void qifen_interval_intlab_mul_scalar(qifen_interval_t dest, const qifen_interval_t a, double b);
void qifen_interval_intlab_sin(qifen_interval_t dest, const qifen_interval_t a);
void qifen_interval_intlab_cos(qifen_interval_t dest, const qifen_interval_t a);
void qifen_interval_intlab_pi(qifen_interval_t dest);
void qifen_interval_to_intlab(const qifen_interval_t v, mxArray **dest);
#endif

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)
inline ::std::pair<double, double> qifen_interval_get_infsup(const qifen_interval_t v)
{
	double a, b;
	::qifen_interval_get_infsup(v, &a, &b);
	return ::std::make_pair(a, b);
}

inline ::std::pair<double, double> qifen_interval_get_midrad(const qifen_interval_t v)
{
	double a, b;
	::qifen_interval_get_midrad(v, &a, &b);
	return ::std::make_pair(a, b);
}
#endif
