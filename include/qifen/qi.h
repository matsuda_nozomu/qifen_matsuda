#pragma once

#if !defined(__cplusplus)
#  include <stdbool.h>
#endif

#include <qifen/types.h>
#include <qifen/config.h>

#if defined(__cplusplus)
extern "C" {
#endif

const qifen_qi_inv_config_ptr qifen_qi_inv_config_default();

qifen_qi_ptr qifen_qi_new(qifen_qi_context_t ctx);
void qifen_qi_delete(qifen_qi_context_t ctx, qifen_qi_t v);
void qifen_qi_init(qifen_qi_context_t ctx, qifen_qi_t v);
void qifen_qi_init_interval(qifen_qi_context_t ctx, qifen_qi_t v, const qifen_interval_t a);
void qifen_qi_init_infsup(qifen_qi_context_t ctx, qifen_qi_t v, double inf, double sup);
void qifen_qi_init_infsup_string(qifen_qi_context_t ctx, qifen_qi_t v, const char *inf, const char *sup);
void qifen_qi_clear(qifen_qi_context_t ctx, qifen_qi_t v);
void qifen_qi_set(qifen_qi_context_t ctx, qifen_qi_t dest, const qifen_qi_t v);
void qifen_qi_set_infsup(qifen_qi_context_t ctx, qifen_qi_t v, double inf, double sup);
void qifen_qi_set_infsup_string(qifen_qi_context_t ctx, qifen_qi_t v, const char *inf, const char *sup);
void qifen_qi_get_infsup(qifen_qi_context_t ctx, qifen_qi_t v, double *inf, double *sup);
void qifen_qi_negate(qifen_qi_context_t ctx, qifen_qi_t dest, const qifen_qi_t a);
void qifen_qi_add_scalar(qifen_qi_context_t ctx, qifen_qi_t dest, const qifen_qi_t a, double b);
void qifen_qi_add(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b);
void qifen_qi_sub(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b);
void qifen_qi_mul_scalar(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, double b);
void qifen_qi_mul_interval(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, const qifen_interval_t b);
void qifen_qi_mul(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b);
qifen_error_t qifen_qi_div(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b,
	const qifen_qi_inv_config_t config QIFEN_DEFAULT_ARG(qifen_qi_inv_config_default()));
qifen_error_t qifen_qi_inv(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a,
	const qifen_qi_inv_config_t config QIFEN_DEFAULT_ARG(qifen_qi_inv_config_default()));

#if !defined(QIFEN_CONFIG_DISABLE_KV)
void qifen_qi_kv_negate(qifen_qi_context_t ctx, qifen_qi_t dest, const qifen_qi_t a);
void qifen_qi_kv_add_scalar(qifen_qi_context_t ctx, qifen_qi_t dest, const qifen_qi_t a, double b);
void qifen_qi_kv_add(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b);
void qifen_qi_kv_sub(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b);
void qifen_qi_kv_mul_scalar(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, double b);
void qifen_qi_kv_mul_interval(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, const qifen_interval_t b);
void qifen_qi_kv_mul(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b);
qifen_error_t qifen_qi_kv_div(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b,
	const qifen_qi_inv_config_t config QIFEN_DEFAULT_ARG(qifen_qi_inv_config_default()));
qifen_error_t qifen_qi_kv_inv(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a,
	const qifen_qi_inv_config_t config QIFEN_DEFAULT_ARG(qifen_qi_inv_config_default()));
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
void qifen_qi_intlab_negate(qifen_qi_context_t ctx, qifen_qi_t dest, const qifen_qi_t a);
void qifen_qi_intlab_add_scalar(qifen_qi_context_t ctx, qifen_qi_t dest, const qifen_qi_t a, double b);
void qifen_qi_intlab_add(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b);
void qifen_qi_intlab_sub(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b);
void qifen_qi_intlab_mul_scalar(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, double b);
void qifen_qi_intlab_mul_interval(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, const qifen_qi_interval_t b);
void qifen_qi_intlab_mul(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b);
qifen_error_t qifen_qi_intlab_div(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b,
	const qifen_qi_inv_config_t config QIFEN_DEFAULT_ARG(qifen_qi_inv_config_default()));
qifen_error_t qifen_qi_intlab_inv(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a,
	const qifen_qi_inv_config_t config QIFEN_DEFAULT_ARG(qifen_qi_inv_config_default()));
#endif

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)
inline ::std::pair<double, double> qifen_qi_get_infsup(qifen_qi_context_t ctx, qifen_qi_t v)
{
	double a, b;
	::qifen_qi_get_infsup(ctx, v, &a, &b);
	return ::std::make_pair(a, b);
}

inline qifen_error_t qifen_qi_div(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b,
	const qifen_qi_inv_config_struct &config)
{
	return ::qifen_qi_div(ctx, dest, a, b, &config);
}

inline qifen_error_t qifen_qi_inv(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a,
	const qifen_qi_inv_config_struct &config)
{
	return ::qifen_qi_inv(ctx, dest, a, &config);
}

#  if !defined(QIFEN_CONFIG_DISABLE_KV)
inline qifen_error_t qifen_qi_kv_div(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b,
	const qifen_qi_inv_config_struct &config)
{
	return ::qifen_qi_kv_div(ctx, dest, a, b, &config);
}

inline qifen_error_t qifen_qi_kv_inv(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a,
	const qifen_qi_inv_config_struct &config)
{
	return ::qifen_qi_kv_inv(ctx, dest, a, &config);
}
#  endif

#  if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
inline qifen_error_t qifen_qi_inlab_div(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a, qifen_qi_t b,
	const qifen_qi_inv_config_struct &config)
{
	return ::qifen_qi_inlab_div(ctx, dest, a, b, &config);
}

inline qifen_error_t qifen_qi_inla_inv(qifen_qi_context_t ctx, qifen_qi_t dest, qifen_qi_t a,
	const qifen_qi_inv_config_struct &config)
{
	return ::qifen_qi_intlab_inv(ctx, dest, a, &config);
}
#  endif
#endif
