#pragma once

#if defined(_MSC_VER)
#  define QIFEN_NOOP __noop
#else
#  define QIFEN_NOOP (void)0
#endif

#if !defined(QIFEN_CONFIG_INTERVAL_KV) && !defined(QIFEN_CONFIG_INTERVAL_INTLAB)
#  if !defined(MATLAB_MEX_FILE)
#    define QIFEN_CONFIG_INTERVAL_KV
#  else
#    define QIFEN_CONFIG_INTERVAL_INTLAB
#  endif
#endif

#if !defined(NDEBUG)
#  define QIFEN_CONFIG_STRICT_TYPE_CHECKING 1
#elif !defined(QIFEN_CONFIG_STRICT_TYPE_CHECKING)
#  define QIFEN_CONFIG_STRICT_TYPE_CHECKING 0
#endif

#if !defined(NDEBUG)
#  define QIFEN_CONFIG_STRICT_DIMENSION_CHECKING 1
#elif !defined(QIFEN_CONFIG_STRICT_DIMENSION_CHECKING)
#  define QIFEN_CONFIG_STRICT_DIMENSION_CHECKING 0
#endif

#if QIFEN_CONFIG_STRICT_TYPE_CHECKING
#  define QIFEN_ENSURE_TYPE(type, var) if (!::mxIsClass(var, type)) return 0;
#else
#  define QIFEN_ENSURE_TYPE(type, var) QIFEN_NOOP
#endif

#if QIFEN_CONFIG_STRICT_DIMENSION_CHECKING
#  define QIFEN_ENSURE_SCALAR(value) if (!::mxIsScalar(value)) return 0;
#  define QIFEN_ENSURE_VECTOR(value) { \
       auto ndim = ::mxGetNumberOfDimensions(value); \
       if (ndim > 2) return 0; \
       auto dim = ::mxGetDimensions(value); \
       if (dim[0] != 1 || dim[1] != 1) return 0; \
     }
#else
#  define QIFEN_ENSURE_SCALAR(value) QIFEN_NOOP
#  define QIFEN_ENSURE_VECTOR(value) QIFEN_NOOP
#endif

#if defined(__cplusplus)
#  define QIFEN_DEFAULT_ARG(v) = v
#else
#  define QIFEN_DEFAULT_ARG(v)
#endif
