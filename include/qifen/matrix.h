#pragma once

#include <qifen/config.h>
#include <qifen/types.h>

#if defined(__cplusplus)
extern "C" {
#endif

extern const size_t qifen_sizeof_matrix;

void qifen_matrix_init(qifen_matrix_t v, size_t row, size_t col);
void qifen_matrix_clear(qifen_matrix_t v);
void qifen_matrix_get_size(const qifen_matrix_t v, size_t *nrows, size_t *ncols);
void qifen_matrix_set(qifen_matrix_t dest, const qifen_matrix_t v);
void qifen_matrix_set_entry(qifen_matrix_t dest, size_t row, size_t col, double v);
void qifen_matrix_get_entry(const qifen_matrix_t dest, size_t row, size_t col, double *v);
void qifen_matrix_resize(qifen_matrix_t v, size_t row, size_t col);
void qifen_matrix_transpose(qifen_matrix_t dest, const qifen_matrix_t a);
void qifen_matrix_negate(qifen_matrix_t dest, const qifen_matrix_t a);
void qifen_matrix_add(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b);
void qifen_matrix_sub(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b);
void qifen_matrix_mul(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b);
void qifen_matrix_mul_scalar(qifen_matrix_t dest, const qifen_matrix_t a, double b);
qifen_error_t qifen_matrix_verify_eig_minmax_real(qifen_interval_t dest, const qifen_matrix_t a, qifen_verify_eig_method_t method);

#if !defined(QIFEN_CONFIG_DISABLE_KV)
#  if defined(__cplusplus)
void qifen_matrix_set_kv(qifen_matrix_t dest, ::boost::numeric::ublas::matrix<double> &v);
extern "C++" inline void qifen_matrix_set_kv(qifen_matrix_t dest, ::boost::numeric::ublas::matrix<double> &&v)
{
#    if defined(QIFEN_CONFIG_INTERVAL_KV)
	**dest = ::std::move(v);
#    elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_matrix_set_kv(dest, static_cast<::boost::numeric::ublas::matrix<double>&>(v));
#    endif
}
#  endif
void qifen_matrix_kv_transpose(qifen_matrix_t dest, const qifen_matrix_t a);
void qifen_matrix_kv_negate(qifen_matrix_t dest, const qifen_matrix_t a);
void qifen_matrix_kv_add(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b);
void qifen_matrix_kv_sub(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b);
void qifen_matrix_kv_mul(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b);
void qifen_matrix_kv_mul_scalar(qifen_matrix_t dest, const qifen_matrix_t a, double b);
qifen_error_t qifen_matrix_kv_verify_eig_minmax_real(qifen_interval_t dest, const qifen_matrix_t a, qifen_verify_eig_method_t method);
#  if defined(__cplusplus)
void qifen_matrix_to_kv(const qifen_matrix_t v, ::boost::numeric::ublas::matrix<double> &dest);
#  endif
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
void qifen_matrix_intlab_transpose(qifen_matrix_t dest, const qifen_matrix_t a);
void qifen_matrix_intlab_negate(qifen_matrix_t dest, const qifen_matrix_t a);
void qifen_matrix_intlab_add(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b);
void qifen_matrix_intlab_sub(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b);
void qifen_matrix_intlab_mul(qifen_matrix_t dest, const qifen_matrix_t a, const qifen_matrix_t b);
void qifen_matrix_intlab_mul_scalar(qifen_matrix_t dest, const qifen_matrix_t a, double b);
qifen_error_t qifen_matrix_intlab_verify_eig_minmax_real(qifen_interval_t dest, const qifen_matrix_t a, qifen_verify_eig_method_t method);
void qifen_matrix_to_intlab(const qifen_matrix_t v, mxArray **dest);
#endif

#if defined(__cplusplus)
}
#endif
