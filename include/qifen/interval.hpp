#pragma once

#include <qifen/interval.h>

namespace qifen {
	class interval : public ::qifen_interval_struct {
	public:
		interval(double v)
		{
			::qifen_interval_init_infsup(this, v, v);
		}

		interval(double inf, double sup)
		{
			::qifen_interval_init_infsup(this, inf, sup);
		}

#if !defined(QIFEN_CONFIG_DISABLE_KV)
		interval(const ::kv::interval<double> &v)
		{
			::qifen_interval_init_kv(this, v);
		}
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
		interval(::mxArray v)
		{
			::qifen_interval_init_intlab(this, v);
		}
#endif

		~interval()
		{
			::qifen_interval_clear(this);
		}

		operator ::qifen_interval_ptr()
		{
			return this;
		}

#if !defined(QIFEN_CONFIG_DISABLE_KV)
		operator ::kv::interval<double>()
		{
			auto r = ::qifen_interval_get_infsup(this);
			return { r.first, r.second };
		}
#endif

		interval &operator=(const ::qifen_interval_t &v)
		{
			::qifen_interval_set(this, &v);
			return *this;
		}

		interval &operator=(double v)
		{
			::qifen_interval_set_infsup(this, v, v);
			return *this;
		}

		interval &operator+=(const ::qifen_interval_t &v)
		{
			::qifen_interval_add(this, this, v);
			return *this;
		}

		interval &operator-=(const ::qifen_interval_t &v)
		{
			::qifen_interval_sub(this, this, v);
			return *this;
		}

		interval &operator*=(const ::qifen_interval_t &v)
		{
			::qifen_interval_mul(this, this, v);
			return *this;
		}

		interval &operator/=(const ::qifen_interval_t &v)
		{
			::qifen_interval_div(this, this, v);
			return *this;
		}
	};
}
