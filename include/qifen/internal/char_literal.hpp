#pragma once

#include <type_traits>

namespace qifen {
	namespace internal {
		template <typename T, char C, wchar_t WC, char16_t C16, char32_t C32>
		struct char_literal;
		template <char C, wchar_t WC, char16_t C16, char32_t C32>
		struct char_literal<char, C, WC, C16, C32> : ::std::integral_constant<char, C> {
		};
		template <char C, wchar_t WC, char16_t C16, char32_t C32>
		struct char_literal<wchar_t, C, WC, C16, C32> : ::std::integral_constant<wchar_t, WC> {
		};
		template <char C, wchar_t WC, char16_t C16, char32_t C32>
		struct char_literal<char16_t, C, WC, C16, C32> : ::std::integral_constant<char16_t, C16> {
		};
		template <char C, wchar_t WC, char16_t C16, char32_t C32>
		struct char_literal<char32_t, C, WC, C16, C32> : ::std::integral_constant<char32_t, C32> {
		};
	}
}

#define QIFEN_INTERNAL_CHAR_LITERAL(c, type) \
   (::qifen::internal::char_literal<type, c, L ## c, u ## c, U ## c>::value)
