#pragma once

#include <stdint.h>

#include <qifen/config.h>
#include <qifen/types.h>

#if defined(__cplusplus)
extern "C" {
#endif

extern const size_t qifen_sizeof_interval_matrix;

void qifen_interval_matrix_init(qifen_interval_matrix_t v, size_t nrow, size_t ncol);
void qifen_interval_matrix_clear(qifen_interval_matrix_t v);
void qifen_interval_matrix_set(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a);
qifen_error_t qifen_interval_matrix_set_infsup(qifen_interval_matrix_t dest, const qifen_matrix_t inf, const qifen_matrix_t sup);
void qifen_interval_matrix_set_entry(qifen_interval_matrix_t dest, size_t row, size_t col, qifen_interval_t v);
void qifen_interval_matrix_set_entry_infsup(qifen_interval_matrix_t dest, size_t row, size_t col, double inf, double sup);
void qifen_interval_matrix_get_infsup(const qifen_interval_matrix_t dest, qifen_matrix_t inf, qifen_matrix_t sup);
void qifen_interval_matrix_get_midrad(const qifen_interval_matrix_t dest, qifen_matrix_t mid, qifen_matrix_t rad);
void qifen_interval_matrix_get_entry(const qifen_interval_matrix_t v, size_t row, size_t col, qifen_interval_t dest);
void qifen_interval_matrix_get_entry_infsup(const qifen_interval_matrix_t v, size_t row, size_t col, double *inf, double *sup);
void qifen_interval_matrix_resize(qifen_interval_matrix_t v, size_t row, size_t col);
void qifen_interval_matrix_negate(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a);
void qifen_interval_matrix_add(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b);
void qifen_interval_matrix_add_matrix(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_matrix_t b);
void qifen_interval_matrix_sub(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b);
void qifen_interval_matrix_mul(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b);
void qifen_interval_matrix_inv(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a);

#if !defined(QIFEN_CONFIG_DISABLE_KV)
#  if defined(__cplusplus)
void qifen_interval_matrix_set_entry_kv(qifen_interval_matrix_t dest, size_t row, size_t col, const ::kv::interval<double> &v);
void qifen_interval_matrix_get_entry_kv(const qifen_interval_matrix_t v, size_t row, size_t col, ::kv::interval<double> &dest);
#  endif
void qifen_interval_matrix_kv_negate(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a);
void qifen_interval_matrix_kv_add(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b);
void qifen_interval_matrix_kv_add_matrix(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_matrix_t b);
void qifen_interval_matrix_kv_sub(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b);
void qifen_interval_matrix_kv_mul(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b);
void qifen_interval_matrix_kv_inv(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a);
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
int qifen_interval_matrix_set_entry_intlab(qifen_interval_matrix_t dest, size_t row, size_t col, ::mxArray *v);
void qifen_interval_matrix_intlab_negate(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a);
void qifen_interval_matrix_intlab_add(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b);
void qifen_interval_matrix_intlab_add_matrix(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_matrix_t b);
void qifen_interval_matrix_intlab_sub(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b);
void qifen_interval_matrix_intlab_mul(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b);
void qifen_interval_matrix_intlab_inv(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a);
void qifen_interval_matrix_to_intlab(const qifen_interval_matrix_t v, mxArray **dest);
#endif

#if defined(__cplusplus)
}
#endif
